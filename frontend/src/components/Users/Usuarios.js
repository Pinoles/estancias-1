import React, { Component } from "react";
import UsuarioModal from './layouts/userUpdateModal';
export default class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      userdata:{
          id:"",
          name:"",
          lastname:"",
          email:"",
          telephone:"",
          birthdate:"",
          is_verified:"",
          student:"",
          teacher:"",
          admin:""
        },
      loaded: false,
      placeholder: "Loading",
    };

    this.editUser = this.editUser.bind(this);
    this.changeHandler = this.changeHandler.bind(this);
    this.changeHandlerCB = this.changeHandlerCB.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
    this.deleteHandler = this.deleteHandler.bind(this);
  }

  changeHandler(e) {
    let newData = this.state.userdata;
    newData[e.target.name] = e.target.value;
    this.setState({userdata:newData});
  }
  changeHandlerCB(e) {
    let newData = this.state.userdata;
    newData[e.target.name] = e.target.checked;
    this.setState({userdata: newData});
}

  editUser(user){
    let userjs = JSON.parse(JSON.stringify(user));
    this.setState({userdata:userjs});
  }

  deleteHandler(user){
    const userResponse = window.confirm("Esta seguro de querer borrar el usuario "+user.name);
    if ( userResponse){
      const csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
      fetch("/api/Users/userDelete/"+user.id+"/",{
        method:'DELETE',
        body:"",
        headers:{
          'Content-Type': 'application/json',
          'X-CSRFToken':csrf_token
        }
      }).then(res => {
        if (res.status > 400) {
          this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return res.json();
      })
      .catch(error => console.error('Error:', error))
      .then(response =>
        {
          window.location.reload(true);
        }
      );
    }
  }

  submitHandler(e){
    e.preventDefault();
    //
    const newData = this.state.userdata;
    const csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    fetch("/api/Users/userUpdate/"+newData['id']+"/", {
        method: 'PUT', // or 'PUT'
        body: JSON.stringify(this.state.userdata), // data can be `string` or {object}!
        headers:{
          'Content-Type': 'application/json',
          'X-CSRFToken':csrf_token
        },
      }).then(res => {
        if (res.status > 400) {
          return this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return res.json();
      })
      .catch(error => console.error('Error:', error))
      .then(response =>
        {if(!response.errors){
          window.location.reload(true);
          }else{
            console.log(response);
          }}
      );
  }

  componentDidMount() {
    fetch("/api/Users/userList/")
      .then(response => {
        if (response.status > 400) {
          return this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            data,
            loaded: true
          };
        });
      });
  }

  render() {
    console.log(this.state);
    return (
      <div>
        <h1>Administración de Usuarios</h1>
        <a href="http://127.0.0.1:8000/usuarios/nuevo">
          <button  style={{color:"white",backgroundColor:"#355F8D"}} type="button" className="btn btn">
            Nuevo Usuario
          </button>
        </a>
        <br></br>
        <br></br>
       
        <div className="modal fade" id="exampleModalScrollable" tabIndex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
         <UsuarioModal 
            data={this.state.userdata}
            changeHandler = {this.changeHandler}
            changeHandlerCB = {this.changeHandlerCB}
            submitHandler = {this.submitHandler}
         ></UsuarioModal>
         </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">Nombre</th>
              <th scope="col">Apellidos</th>
              <th scope="col">Email</th>
              <th scope="col">Telefono</th>
              <th scope="col">Nacimiento</th>
              <th scope="col">Verificado</th>
              <th scope="col">Es Alumno</th>
              <th scope="col">Es Profesor</th>
              <th scope="col">Es Admin</th>
              <th scope="col">Acciones</th>
            </tr>
          </thead>
            <tbody>
              {this.state.data.map(user => {
              return (
                  <tr key={user['id']}>
                      <th scope="row">{user['name']}</th>
                      <th scope="row">{user['lastname']}</th>
                      <th scope="row">{user['email']}</th>
                      <th scope="row">{user['telephone']}</th>
                      <th scope="row">{user['birthdate']}</th>
                      <th scope="row">
                        <input type="checkbox" 
                          defaultChecked={user['is_verified']} disabled>
                        </input>
                      </th>
                      <th scope="row">
                        <input type="checkbox" 
                          defaultChecked={user['student']} disabled>
                        </input>
                      </th>
                      <th scope="row">
                        <input type="checkbox" 
                          defaultChecked={user['teacher']} disabled>
                        </input>
                      </th>
                      <th scope="row">
                        <input type="checkbox" 
                          defaultChecked={user['admin']} disabled>
                        </input>
                      </th>
                      <th scope="row">
                        <button style={{color:"white",backgroundColor:"#355F8D"}} className="btn btn-default" onClick={() => this.editUser(user)} data-toggle="modal" data-target="#exampleModalScrollable">
                          Editar
                        </button>
                        &nbsp;
                        <button style={{color:"white",backgroundColor:"#961429"}}className="btn" onClick={() => this.deleteHandler(user)}>
                          Borrar
                        </button>
                      </th>
                  </tr>
              );
              })}
            </tbody>
        </table>
      </div>
    );
  }
}