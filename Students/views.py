from django.shortcuts import render,redirect
from .models import Students, StudentsEnrollment
from .serializer import  StudentsSerializer, StudentsEnrollmentSerializer, GETStudentsEnrollmentSerializer
from Payments.serializer import PaymentsSerializer
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response

from django.template.loader import render_to_string
from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa
from django.utils.html import strip_tags
from django.core.mail import EmailMultiAlternatives
import datetime
import requests

from cele.email_config import EMAIL_HOST_USER


@api_view(['GET'])
def apiOverview(request):
    api_urls={
        'List':'/Students-List/',
        'Detail-View':'/Students-Detail/<str:pk>/',
        'Create':'/Students-Create/',
        'Update':'/Students-Update/<str:pk>/',
        'Delete':'/Students-Delete/<str:pk>/',
        'Update Verificate':'/Students-Verificate/<str:pk>',
    }
    return Response(api_urls)

@api_view(['GET'])
def StudentsList(request):
    queryset = Students.objects.all()
    serializer = StudentsSerializer(queryset,many=True)
    return Response(serializer.data)

@api_view(['GET'])
def StudentsDetail(request,pk):
    Lenguage = Students.objects.get(group_id=pk)
    serializer = StudentsSerializer(Lenguage,many=False)
    return Response(serializer.data)

@api_view(['POST'])
def StudentsCreate(request):
    serializer = StudentsSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['GET'])
def apiOverview2(request):
    api_urls={
        'List':'/StudentsEnrollment-List/',
        'Detail-View':'/StudentsEnrollment-Detail/<str:pk>/',
        'Create':'/StudentsEnrollment-Create/',
        'Update':'/StudentsEnrollment-Update/<str:pk>/',
        'Delete':'/StudentsEnrollment-Delete/<str:pk>/',
        'Update Verificate':'/StudentsEnrollment-Verificate/<str:pk>',
    }
    return Response(api_urls)

@api_view(['GET'])
def StudentsEnrollmentList(request):
    queryset = StudentsEnrollment.objects.all()
    serializer = GETStudentsEnrollmentSerializer(queryset,many=True)
    return Response(serializer.data)

@api_view(['GET'])
def StudentsEnrollmentDetail(request,pk):
    Lenguage = StudentsEnrollment.objects.get(group_id=pk)
    serializer = StudentsEnrollmentSerializer(Lenguage,many=False)
    return Response(serializer.data)

@api_view(['POST'])
def StudentsEnrollmentCreate(request):
    serializer = StudentsEnrollmentSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        sEnroll = StudentsEnrollment.objects.get(user_id=serializer.data['user_id'],
            group_id=serializer.data['group_id'])
        sEnrollSerial = GETStudentsEnrollmentSerializer(sEnroll,many=False).data

        if sEnrollSerial['group_id']['course_type'] == "M1" or sEnrollSerial['group_id']['course_type'] == "M2":
            presio = 1500
        else:
            presio = 1300

        now = datetime.datetime.now()+datetime.timedelta(days=3)
        timestamp = int( datetime.datetime.timestamp(now))

        url = "https://api.conekta.io/checkouts"
        headers={'accept':'application/vnd.conekta-v2.0.0+json','content-type':'application/json'}
        data='{"name":"Pago inscripcion","type":"PaymentLink","recurrent":false,"expired_at":'+str(timestamp)+',"allowed_payment_methods": ["cash", "card", "bank_transfer"],"needs_shipping_contact": true,"order_template": {"line_items": [{"name":"'+sEnrollSerial["group_id"]["desc_group"]+'","unit_price": '+str(presio*100)+',"quantity": 1}],"currency": "MXN","customer_info": {"name": "'+sEnrollSerial['user_id']['name']+'","email":"'+sEnrollSerial["user_id"]["email"]+'","phone":"'+sEnrollSerial["user_id"]["telephone"]+'"}}}'
        key = 'key_dazj6vCqmaJ7kqXS75tBUw'

        resp = requests.post(url=url,data=data,headers=headers,auth=(key,""))
        if resp.status_code <400:
            resp = resp.json()
            send_email(sEnrollSerial['user_id'],resp['url'])
            return Response({'errors':None,'data':resp['url']},status=200)
        return Response({'errrors':resp.json()},status=422)
    return Response({'errors':serializer.errors},status=400)

def send_email(user,link):
    try:
        html_content = render_to_string('emailpay.html',{'pk':link})
        text_content = strip_tags(html_content)
        email = EmailMultiAlternatives(
            'Verificacion',
            text_content,
            EMAIL_HOST_USER,
            [user['email']],
        )
        email.attach_alternative(html_content,'text/html')
        email.send()
        return True
    except NameError:
        print(NameError)
        return False
"""
        template_path = 'comprobante.html'

        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="Report.pdf"'

        html = render_to_string(template_path, {'report': sEnrollSerial})
        print (html)

        pisaStatus = pisa.CreatePDF(html, dest=response)
"""