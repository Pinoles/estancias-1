from django.shortcuts import render,redirect
from django.urls import reverse
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.mail import EmailMultiAlternatives
from datetime import datetime
from django.contrib.auth import login, logout
from .models import Users
from .serializers import (UsersSerializer,
    UsersLogin,
    UserRegistrationSerializer)
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from cele.email_config import EMAIL_HOST_USER
from .backends import UserBackend
from django.http import HttpResponseRedirect

@api_view(['GET'])
def apiOverview(request):
    api_urls={
        'List':'/User-List/',
        'Detail View':'/User-Detail/<str:pk>/',
        'Create':'/User-Create/',
        'Update':'/User-Update/<str:pk>/',
        'Delete':'/User-Delete/<str:pk>/',
        'Update Verificate':'/User-Verificate/<str:pk>',
    }
    return Response(api_urls)

@api_view(['GET'])
def userList(request):
    userlist = Users.objects.all()
    serializer = UsersSerializer(userlist,many=True)
    return Response(serializer.data)

@api_view(['GET'])
def userDetail(request,pk):
    user = Users.objects.get(id=pk)
    if user is not None:
        serializer = UsersSerializer(user,many=False)
        return Response(serializer.data)
    return Response({'errors':'usuario no existe'},status=status.HTTP_422_UNPROCESSABLE_ENTITY)

@api_view(['GET'])
def userDetailAuth(request):
    if request.user.is_authenticated:
        user = Users.objects.get(id=request.user.id)
        if user is not None:
            serializer = UsersSerializer(user,many=False)
            return Response(serializer.data,status=200)
        return Response({'errors':'perfil no existe'},status=422)
    return Response({'errors':'usuario no autenticado','admin':False },status=status.HTTP_406_NOT_ACCEPTABLE)

@api_view(['GET'])
def userLoget(request):
    if request.user.is_authenticated:
        user = Users.objects.get(email=request.user.email)
        serializer = UsersSerializer(user,many=False)
        return Response(serializer.data,status=200)
    return Response({'errors':'usuario no autenticado','admin':False },status=status.HTTP_406_NOT_ACCEPTABLE)

@api_view(['POST'])
def userCreate(request):
    serializer = UserRegistrationSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        send_email(serializer.data)
        #login(request,user,backend='django.contrib.auth.backends.ModelBackend')
        return Response({'errors':None,'user':serializer.data}, status=200)
    return Response({'errors':serializer.errors}, status=status.HTTP_406_NOT_ACCEPTABLE)

def send_email(user):
    try:
        html_content = render_to_string('email.html',{'pk':user.id})
        text_content = strip_tags(html_content)
        email = EmailMultiAlternatives(
            'Verificacion',
            text_content,
            EMAIL_HOST_USER,
            [user.email],
        )
        email.attach_alternative(html_content,'text/html')
        email.send()
    except:
        pass

@api_view(['PUT'])
def userUpdate(request,pk):
    user = Users.objects.get(id=pk)
    serializer = UsersSerializer(instance=user,data=request.data,partial=True)
    if serializer.is_valid():
        serializer.save()
        return Response({'errors':None,'user':serializer.data}, status=200)
    return Response({'errors':serializer.errors}, status=status.HTTP_406_NOT_ACCEPTABLE)

@api_view(['DELETE'])
def userDelete(request,pk):
    try:
        user = Users.objects.get(id=pk)
        if( user is not None):
            user.delete()
            return Response({'errors':None},status=status.HTTP_204_NO_CONTENT)
        return Response({'errors':"usuario no encontrado"},status=status.HTTP_404_NOT_FOUND)
    except:
        return Response({'errors':'algo ocurrio'},status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    

@api_view(['GET'])
def userVerificate(request,pk):
    user = Users.objects.get(id=pk)
    serializer = UsersSerializer(instance=user,data={'is_verified':True,'verified_date':datetime.now().strftime("%Y-%m-%d")},partial=True)
    if serializer.is_valid():
        serializer.save()
        return HttpResponseRedirect(reverse('dashboard'))
    return Response(serializer.data)

@api_view(['POST'])
def userAuth(request):
    user_backend = UserBackend()
    userializer = UsersLogin(data=request.data)
    if userializer.is_valid():
        user = user_backend.authenticate(request=None,email=request.data['email'],password=request.data['password'])
        if user is not None:
            login(request,user,backend='django.contrib.auth.backends.ModelBackend')
            return Response({'errors':None,'id':user.id},status=status.HTTP_200_OK)
        else:
            return Response({'errors':{'email': ['El usuario o contraseña no son correctos']}}, status=422)
    return Response({'errors':userializer.errors}, status=status.HTTP_406_NOT_ACCEPTABLE)

@api_view(['POST'])
def userLogout(request):
    logout(request)
    return Response(status=status.HTTP_200_OK)

@api_view(['GET'])
def userTeacher(request):
    users = Users.objects.filter(teacher=True)
    serializer = UsersSerializer(users,many=True)
    return Response(serializer.data)