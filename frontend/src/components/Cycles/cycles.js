import React, { Component } from "react";
import {Link} from 'react-router-dom';

export default class cycles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loaded: false,
      placeholder: "Loading"
    };
  }

  componentDidMount() {
    fetch("/api/Cycles/cycles-list/")
      .then(response => {
        if (response.status > 400) {
          return this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            data,
            loaded: true
          };
        });
      });
  }

  render() {
    return (
      <div>
        <h1>Administración de ciclos</h1>
        <Link to="./nuevo">
          <button style={{color:"white",backgroundColor:"#355F8D"}} type="button" className="btn btn">
            Nuevo Ciclos
          </button>
          <br></br>   <br></br>
        </Link>
        <table className="table table-striped">
            <thead>
            <tr>
              <th scope="col">Descripcion</th>
              <th scope="col">Fecha de inicio</th>
              <th scope="col">Fecha de Fin</th>
              <th scope="col">Cambios</th>
            </tr>
          </thead>
            <tbody>
                  {this.state.data.map(cycle => {
                  return (
                      <tr key={cycle['cycle_id']}>
                          <th scope="row">{cycle['desc_cycle']}</th>
                          <th scope="row">{cycle['start_date']}</th>
                          <th scope="row">{cycle['end_date']}</th>
                          <th scope="row">
                            <input type="checkbox" 
                              defaultChecked={cycle['allow_change']} disabled>
                            </input>
                          </th>
                      </tr>
                  );
                  })}
              </tbody>
        </table>
      </div>
    );
  }
}