import React from 'react';

export default function PagoExamen (props){
    return(
        <div className="mt-2 mt-3"  id="content-wrapper">
            <div style={{fontSize:"120%"}}><p><b>Pago de Inscripción</b></p></div>
            <div className="ml-2"> <div style={{backgroundColor:"#FFF"}}><p style={{padding:"10px"}}><b>Métodos de pago</b></p></div>
            <div style={{fontSize:"100%",textAlign:"center"}}><p><b>Puedes elegir entre las diferentes formas de pago para cubrir tu inscripción</b></p></div>
            <div className="card-deck">
  <div className="card">
      <div  style={{backgroundColor:"#fff", textAlign:"center",padding:"30px"}}>
      <div  style={{backgroundColor:"#bcffc3", textAlign:"center"}}> <img src="https://didactiko.mx/wp-content/uploads/2020/01/meses-sin-intereses.png"  width="200px" height="200px"></img> </div>  

<div  style={{color:"#2698fb",backgroundColor:"#e8f4f9"}} className="card-body">
  <h5 className="card-title">Pago en Linea</h5>
  <p className="card-text">Paga directamente en este portal utilizando una cuenta bancaria</p>
  <br></br>
  <div style={{textAlign:"center"}}>  <a href="#" className="btn btn-primary" >Realizar pago</a> </div>
 
</div>
      </div>
  
   
  </div>
  <div className="card">
  <div  style={{backgroundColor:"#fff", textAlign:"center",padding:"30px"}}>
  <div  style={{backgroundColor:"#bcffc3", textAlign:"center"}}> <img src="https://leapinternet.hsbc.com.mx/LeapInternetV2/resources/images/hsbc_logo_1.png"  width="200px" height="200px"></img> </div>  

<div  style={{color:"#2698fb",backgroundColor:"#e8f4f9"}} className="card-body">
  <h5 className="card-title">Depósito o transferencia</h5>
  <p className="card-text">Descarga e imprime tu formato de prepago bancario</p>

  <br></br>
  <div style={{textAlign:"center", fontSize:"15px"}} className="icon ion-md-document lead mr-2" >Formato de prepago bancario </div>
  <br></br>
    </div>
    </div>
  </div>

  <div className="card">
      <div  style={{backgroundColor:"#fff",padding:"30px"}}>
  <div className="card-header">
      <small className="text-muted"><h5>Cantidad a pagar</h5></small>
    </div>
    <div className="card-body">
      <p className="card-text">Inscripción: </p>
      <p className="card-text">Descuento: </p>
      <p className="card-text">Prórroga: </p>
      <br></br>
      <p className="card-text">Total a pagar: </p>
        <br></br>
        <p className="card-text">Pago realizago: </p>
        <p className="card-text">Faltante: </p>
    </div>
    
  </div>
  </div>
</div></div>
    
        </div>

    );
}