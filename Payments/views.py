from django.shortcuts import render
from .serializer import PaymentsSerializer
from .serializer import PaymentMethodsSerializer
from .models import Payments
from .models import PaymentMethods

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

import json
# Create your views here.
@api_view(['GET'])
def apiOverview(request):
    api_urls={
        'List':'/Payments-List/',
        'Detail View':'/Payments-Detail/<str:pk>/',
        'Detail View':'/PaymentMethods-Detail/<str:pk>/',
        'Create':'/Payments-Create/',
        'Create':'/PaymentMethods-Create/',
        'Update':'/Payments-Update/<str:pk>/',
        'Delete':'/Payments-Delete/<str:pk>/',
        'Update Verificate':'/Payments-Verificate/<str:pk>',
    }
    return Response(api_urls)

@api_view(['GET'])
def PaymentsList(request):
    queryset = Payments.objects.all()
    serializer = PaymentsSerializer(queryset,many=True)
    return Response(serializer.data)

@api_view(['GET'])
def PaymentsDetail(request,pk):
    queryset = Payments.objects.get(lenguage_id=pk)
    serializer = PaymentsSerializer(queryset,many=False)
    return Response(serializer.data)

@api_view(['GET'])
def PaymentMethodsDetail(request,pk):
    queryset = PaymentMethods.objects.get(lenguage_id=pk)
    serializer = PaymentMethodsSerializer(queryset,many=False)
    return Response(serializer.data)

@api_view(['POST'])
def PaymentsCreate(request):
    serializer =PaymentsSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['POST'])
def PaymentMethodsCreate(request):
    serializer =PaymentMethodsSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

@api_view(['POST'])
def PaymentConekta(request):
    data = request.data
    if data['type'] == 'charge.paid':
        return Response( status=200)
    return Response(status=400)