import React, { Component } from 'react'
import Teachers from '../Groups/layouts/Teachers';

export default class teacherCreate extends Component{
    constructor(props){
        super(props);
        this.state = {
            user_id:"0",
            data:[],
            placeholder:"Loading",
            loaded:false
        };

        this.changeHandler = this.changeHandler.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
    }

    componentDidMount() {
        fetch("/api/Users/userTeacher/")
          .then(response => {
            if (response.status > 400) {
              return this.setState(() => {
                return { placeholder: "Something went wrong!" };
              });
            }
            return response.json();
          })
          .then(data => {
            this.setState(() => {
              return {
                data,
                loaded: true
              };
            });
          });
      }

    changeHandler(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    submitHandler(e){
        e.preventDefault();
        console.log(this.state.user_id);
        const csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        if(this.state.user_id=="0") return;
        fetch("/api/Teachers/teacher-Create/", {
            method: 'POST', // or 'PUT'
            body: JSON.stringify({user_id:this.state.user_id}), // data can be `string` or {object}!
            headers:{
                'Content-Type': 'application/json',
                'X-CSRFToken':csrf_token
            },
            
            }).then(res => {
            if (res.status > 400) {
                this.setState(() => {
                return { placeholder: "Something went wrong!" };
                });
            }
            return res.json();
            })
            .catch(error => console.error('Error:', error))
            .then(response =>
            {if(!response.errors){
                console.log(response);
                this.props.history.goBack();
                }else{
                console.log(response);
                }}
            );
    
    }

    render(){
        const {user_id} = this.state;

        return(
            <div>
               
               <form style={{marginTop: "80px",marginBottom: "80px",background:"#EEEEEE"}} className="card text-center" onSubmit={this.submitHandler} >

            <div  className="card text-center card bg-light col-md-8" style={{maxWidth: "680px",marginLeft: "340px"}}>
                <h5 className="card-header">Registrar al nuevo profesor</h5>
                <div   style={{maxWidth: "680px"}} className="card-body">
                    <div className="col">
                        <select className="form-control" value={user_id} name="user_id" onChange={this.changeHandler}>
                            <option value="" >-seleccione-</option>
                            {this.state.data.map(teacher => {
                                return (
                                <option key={teacher.id} value={teacher.id}>{teacher.name} {teacher.lastname}</option>
                                );
                                })
                            }
                        </select>
                    </div>
                    <div className="form-group row">
                        <br></br> <br></br>
                        <div className="col-sm-12 text-center">
                        <button text-align="center" type="submit" className="btn btn-outline-danger btn-lg center-block">Registrar</button>
                        </div>
                    </div>
                </div>
            </div>
            </form> 
            </div>
        );
    }
}