from rest_framework import serializers
from .models import LenguagesCourses

class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = LenguagesCourses
        fields = ('__all__')

class GETLanguageSerializer(serializers.ModelSerializer):
    cordinated_by = serializers.StringRelatedField(many=False)
    class Meta:
        model = LenguagesCourses
        fields = ('__all__')