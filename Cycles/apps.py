from django.apps import AppConfig


class CyclesConfig(AppConfig):
    name = 'Cycles'
