from django.db import models
# Create your models here.
dict_error_messages = {'max_length':'muy largo','blank':'no se puede dejar en blanco'}
class LenguagesCourses (models.Model):
    lenguage_id=models.AutoField(auto_created=True,primary_key=True)
    desc_lenguage=models.CharField('descripcion de curso',max_length=50,error_messages=dict_error_messages)
    cordinated_by=models.ForeignKey('Users.Users',on_delete = models.SET_NULL, null = True)

    def __str__(self):
        return self.desc_lenguage