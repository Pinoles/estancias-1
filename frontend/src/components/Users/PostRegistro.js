import React, { Component } from 'react'

export default class PostRegistro extends Component{
    constructor(props){
        super(props);
        this.state = {
            name:"",
            lastname:"",
            email:"",
            password:"",
            confimpassword:"",
            telephone:"",
            birthdate:"2018-07-22",
            user_id:"",
            is_verified:false,
            student:true,
            teacher:false,
            admin:false
        };

        this.changeHandler = this.changeHandler.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
    }

    changeHandler(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    submitHandler(e){
        e.preventDefault();
        console.log(this.state);
        const csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        if(this.state.confimpassword == this.state.password){
            fetch("/api/Users/userCreate/", {
                method: 'POST', // or 'PUT'
                body: JSON.stringify(this.state), // data can be `string` or {object}!
                headers:{
                  'Content-Type': 'application/json',
                  'X-CSRFToken':csrf_token
                },
                
              }).then(res => {
                if (res.status > 400) {
                  return this.setState(() => {
                    return { placeholder: "Something went wrong!" };
                  });
                }
                return res.json();
              })
              .catch(error => console.error('Error:', error))
              .then(response =>
                {if(!response.errors){
                    window.location.href = '../';
                  }else{
                    console.log(response)
                  }}
              );
        }
    }

    render(){
        const {name,lastname,email,password,confimpassword,telephone,birthdate} = this.state;

        return(        
             <div className="row justify-content-center">
                 <div className="col-8">
                    <form style={{backgroundColor:"#fff"}} className="card text-center mt-2 mb-2" onSubmit={this.submitHandler} >

<div  className="card text-left  col-md-8" style={{padding:"0",maxWidth:"100%"}}>
    <h5 className="card-header text-center" style={{backgroundColor:"#fff"}}>Registro</h5>
    <div className="card-body">
        <div  className="form-group ">
            <label htmlFor="inputName" ><b>Primer Nombre</b></label>
            <input type="text" 
                className="form-control" 
                id="inputName" name="name" 
                placeholder="Primer Nombre" 
                value={name}
                onChange={this.changeHandler} />
        </div>
        <div className="form-group ">
            <label htmlFor="inputLSName"><b>Apellidos</b></label>
            
            <input type="text"
                className="form-control" 
                id="inputLastName" 
                name="lastname" 
                placeholder="Apellidos" 
                value={lastname}
                onChange={this.changeHandler} />
            
        </div>
        <div className="form-group ">
            <label htmlFor="inputEmail" ><b>Correo Electronico</b></label>
            
            <input type="email" 
                className="form-control" 
                id="inputEmail3" 
                name="email" 
                placeholder="Correo Electronico" 
                value={email}
                onChange={this.changeHandler} />
            
        </div>
        <div className="form-group ">
            <label htmlFor="inputPassword" ><b>Contraseña</b></label>
            
            <input type="password" 
                className="form-control" 
                id="inputPassword" 
                name="password" 
                placeholder="Contraseña" 
                value={password}
                onChange={this.changeHandler} />
            
        </div>
        <div className="form-group ">
            <label htmlFor="inputPassword" ><b>Confirmar Contraseña</b></label>
            
            <input type="password" 
                className="form-control" 
                id="inputConfirmPassword" 
                name="confimpassword" 
                placeholder="Confirmar Contraseña"
                value={confimpassword}
                onChange={this.changeHandler} />
            
        </div>
        <div className="form-group ">
            <label htmlFor="inputTEL" ><b>Numero Telefonico</b></label>
            <input 
                type="tel" 
                className="form-control" 
                id="inputTEL"
                name="telephone" 
                value={telephone} 
                onChange={this.changeHandler} 
                placeholder="Numero Telefonico"
            />
            
        </div>
        <div className="form-group ">
            <label htmlFor="inputBirthday" ><b>Fecha de Nacimiento</b></label>
            <input className="col form-control" type="date" id="start" name="birthdate"
            value={birthdate}
            min="1960-01-01" max="2020-12-31"
            onChange={this.changeHandler} />
        </div>
        <div className="form-group ">
            <div className="col-sm-12 text-center">
            <button style={{backgroundColor:"#b41c34",
                               color: "#fff"}}
                    text-align="center" 
                    type="submit" 
                    className="btn btn-lg center-block">Registrar</button> 
            </div>
        </div>
    </div>
</div>
</form>
                 </div>
             </div>
        );
    }
}