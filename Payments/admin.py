from django.contrib import admin
from .models import PaymentMethods, Payments
# Register your models here.
modelos = [Payments, PaymentMethods]
admin.site.register(modelos)