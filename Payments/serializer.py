from rest_framework import serializers
from .models import Payments
from .models import PaymentMethods

class PaymentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payments
        fields = ('__all__')

class PaymentMethodsSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentMethods
        fields = ('_all_')