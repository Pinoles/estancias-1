import React from 'react';

export default function GroupModal(props){
    return(
        <div className="modal-dialog modal-dialog-scrollable" role="document">
            <div className="modal-content">
                <div className="modal-header">
                    <a><b>Detalles del curso</b></a>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body">
                    <div>
                        <div className="row">
                            <div className="col">
                            <label className="col-sm-2 col-form-label">Descripcion</label>
                            </div>
                            <div className="col">
                            <label className="col-sm-2 col-form-label">{props.data.desc_group}</label>
                            </div>
                            
                            
                        </div>
                        <div className="row">
                            <div className="col">
                            <label className="col-sm-2 col-form-label">Lenguaje</label>
                            </div>
                            <div className="col">
                            <label className="col-sm-2 col-form-label">{props.data.lenguage_id}</label>
                        </div>
                            </div>
                            
                            
                        <div className="row">
                            <div className="col">
                            <label className="col-sm-2 col-form-label">Ciclo</label>
                            </div>
                            <div className="col">
                            <label className="col-sm-2 col-form-label">{props.data.cycle_id}</label>
                            </div>
                            
                            
                        </div>
                        <div className="row">
                            <div className="col">
                                <label className="col-sm-2 col-form-label">Capacidad</label>
                            </div>
                            <div className="col">
                                <label className="col-sm-2 col-form-label">{props.data.max_capacity}</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <label className="col-sm-2 col-form-label">Nivel</label>
                            </div>
                            <div className="col">
                                <label className="col-sm-2 col-form-label">{props.data.level}</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <label className="col-sm-2 col-form-label">Aula</label>
                            </div>
                            <div className="col">
                                <label className="col-sm-2 col-form-label">{props.data.room}</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <label className="col-sm-2 col-form-label">Profesores</label>
                            </div>
                            <div className="col">
                                <label className="col-sm-2 col-form-label">{props.data.teacher_id}</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <label className="col-sm-2 col-form-label">Tipo de Curso</label>
                            </div>
                            <div className="col">
                                <label className="col-sm-2 col-form-label">{props.data.course_type}</label>
                            </div>
                        </div>
                    </div>
                </div>   
                <div className="modal-footer">
                <button style={{color:"white",backgroundColor:"#961429"}} type="button" className="btn btn" data-dismiss="modal">Cancelar</button>
                <button  style={{color:"white",backgroundColor:"#355F8D"}} type="button" className="btn btn" data-dismiss="modal" onClick={props.submitHandler}>Inscribirse</button>
            </div>  
            </div>
            
        </div>
    );
}
