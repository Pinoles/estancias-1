from django.db import models
# Create your models here.
dict_error_messages = {'max_length':'muy largo','blank':'no se puede dejar en blanco'}

class Groups(models.Model):
    course_type_choices=[('M1','Semanal'),('M2','Sabatino'),('M3','Kids'),('M4','Auto Aprendizaje')]

    group_id=models.AutoField(auto_created=True,primary_key=True)
    desc_group=models.CharField('descripcion de grupo',max_length=50,error_messages=dict_error_messages)
    lenguage_id=models.ForeignKey('Lenguages.LenguagesCourses',on_delete=models.SET_NULL, null = True)
    cycle_id=models.ForeignKey('Cycles.Cycles',on_delete=models.SET_NULL, null = True)
    max_capacity=models.IntegerField('capacidad de grupo')
    level=models.IntegerField('nivel del grupo')
    room=models.CharField('aula perteneciente',max_length=50,error_messages=dict_error_messages)
    teacher_id=models.ForeignKey('Teachers.Teachers',on_delete=models.SET_NULL, null = True)
    course_type=models.CharField(choices=course_type_choices,default='M1',max_length=2)
    
    def __str__(self):
        return self.desc_group

class GroupsSchedule(models.Model):
    day_week_choices=[('Mo','Lunes'),('Tu','Martes'),('We','Miercoles'),('Th','Jueves'),('Fr','Viernes'),('Sa','Sabado'),('Su','Domingo')]
    group_schedule_id=models.AutoField(auto_created=True,primary_key=True)
    group_id=models.ForeignKey('Groups', on_delete=models.SET_NULL,null = True,related_name='groupSh')
    day_of_week=models.CharField('dia de la semana',choices=day_week_choices,default='Su',max_length=2)
    start_time=models.TimeField()
    end_time=models.TimeField()

    def __str__(self):
        full_name = '%s %s-%s' % (self.get_day_of_week_display(), self.start_time,self.end_time)
        return full_name.strip()

class PreRegisterGroup(models.Model):
    pre_register_id=models.AutoField(auto_created=True,primary_key=True)
    group_id=models.ForeignKey('Groups', on_delete=models.SET_NULL,null = True,related_name='preregister')
    user_id=models.ForeignKey('Users.Users', on_delete=models.SET_NULL,null=True)
    payment=models.BooleanField('si a pagado', default=False)
    approved=models.BooleanField('si paso el examen', default=False)

    class Meta:
        unique_together=('user_id','group_id')

    def __str__(self):
        full_name = '%s %s' % (self.group_id, self.user_id)
        return full_name.strip()
    