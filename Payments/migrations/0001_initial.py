# Generated by Django 3.0.7 on 2020-06-26 00:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('Discounts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PaymentMethods',
            fields=[
                ('payment_method_id', models.AutoField(auto_created=True, primary_key=True, serialize=False)),
                ('desc_method', models.CharField(error_messages={'blank': 'no se puede dejar en blanco', 'max_length': 'muy largo'}, max_length=20, verbose_name='descripcion de metodo')),
            ],
        ),
        migrations.CreateModel(
            name='Payments',
            fields=[
                ('payment_id', models.AutoField(auto_created=True, primary_key=True, serialize=False)),
                ('payment_date', models.DateField(auto_now_add=True)),
                ('payment_reference', models.CharField(error_messages={'blank': 'no se puede dejar en blanco', 'max_length': 'muy largo'}, max_length=30, unique=True, verbose_name='referencia de pago')),
                ('ammount', models.DecimalField(decimal_places=3, max_digits=5, verbose_name='monto a pagar')),
                ('discount_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='Discounts.Discounts')),
                ('payment_method_id', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='Payments.PaymentMethods')),
            ],
        ),
    ]
