import React,  { Component }from 'react';

export default class Preinscripciongrupo extends Component{
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      pregister:{
        group_id:"",
        user_id:""
        },
      loaded: false,
      placeholder: "Loading",
    };

    this.submitHandler = this.submitHandler.bind(this);
  }

  submitHandler(e){
    e.preventDefault();
    //
    const csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    fetch("/api/admin/preRegister-Create/", {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(this.state.pregister), // data can be `string` or {object}!
        headers:{
          'Content-Type': 'application/json',
          'X-CSRFToken':csrf_token
        },
      }).then(res => {
        if (res.status > 400) {
          this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return res.json();
      })
      .catch(error => console.error('Error:', error))
      .then(response =>
        {if(!response.errors){
            alert('se pre registro exitosamente')
          }else{
            alert('ocurrio un error: '+ JSON.stringify(response.errors))
          }}
      );
  }

  componentDidMount() {
    let id = this.props.match.params.id;
    fetch("/api/admin/group-Detail/"+id)
      .then(response => {
        if (response.status > 400) {
          this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            data,
            loaded: true
          };
        });
      });

      fetch("/api/Users/userLoget")
      .then(response => {
        if (response.status > 400) {
          this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        const pregister = {
          group_id:id,
          user_id:data.id
          };
        this.setState(() => {
          return {
            pregister,
            loaded: true
          };
        });
      });
  }

  render(){
    return(
      <div style={{marginTop:"4%", marginLeft:"4%"}}  id="content-wrapper"> 
        <div style={{backgroundColor:"#fff",fontSize:"30px",padding:"5%"}}><b>{this.state.data.desc_group} </b>- nivel {this.state.data.level}
        <br></br>
        <hr></hr>
      <form style={{backgroundColor:"#eeeeee",fontSize:"70%"}}>
   <fieldset disabled>
   <div className="form-group">
     <label style={{padding:"10px"}} htmlFor="disabledTextInput">Idioma</label>
    <label style={{padding:"10px"}}>{this.state.data.lenguage_id}</label>
   </div>
   <hr></hr>
   <div className="form-group">
   <label style={{padding:"10px"}} htmlFor="disabledTextInput">Profesor</label>
    <label style={{padding:"10px"}}>{this.state.data.teacher_id}</label>
   </div>
   <hr></hr>
   <div className="form-group">
     <label style={{padding:"10px"}} htmlFor="disabledTextInput">Tipo de curso</label>
     <label style={{padding:"10px"}}>{this.state.data.course_type}</label> 
   </div>
   <hr></hr>
   <div className="form-group">
   <label style={{padding:"10px"}} htmlFor="disabledTextInput">Horario</label>
   <label style={{padding:"10px"}}>{this.state.data.groupSh? this.state.data.groupSh[0]:null}</label> 
   </div>
   <hr></hr>
   <div className="form-group">
   <label style={{padding:"10px"}} htmlFor="disabledTextInput">Salón</label>
    <label style={{padding:"10px"}}>{this.state.data.room}</label>
   </div>
   <hr></hr>
   <div className="form-group">
   <label style={{padding:"10px"}} htmlFor="disabledTextInput">Cupo</label>
    <label style={{padding:"10px"}}>{this.state.data.max_capacity}</label>
   </div>
 
  
      
   </fieldset>
       </form>
      <br></br>
           <div className="col-sm-12 text-center">
           <button style={{backgroundColor:"#a1132a",
                              color: "#fff", padding:"1%"}}
                   text-align="center" 
                   type="button" className="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Pre-Inscribirme</button> 
           </div>
      
      </div>
      <br></br>
      <br></br>
      <br></br>
      <div style={{backgroundColor:"#fff",fontSize:"30px",padding:"3%"}}>
      
      <div className="container">
   <div className="row">
       <div className="col-md-8"><b>¿El nivel 1 es muy basico para ti? </b>
       <p>Presenta el examen de ubicación para acceder al nivel correspondiente a tu conocimiento</p></div>
       <div className="col-md-4"><div className="col-sm-12 text-center">
       <br></br>
      
      
           <button style={{backgroundColor:"#a1132a",
                              color: "#fff"}}
                   text-align="center" 
                   type="submit" 
                   className="btn btn-lg bottom-right">Examen de ubicación	</button> 
           </div></div>
      
   </div>
</div>
      
      
      
      </div>



<br></br>
<div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
 <div className="modal-dialog" role="document">
   <div className="modal-content">
     <div className="modal-header">
       <h5 style={{textAlign:"center"}}className="modal-title" id="exampleModalLabel"><b>Confirmar Pre-Inscripción</b></h5>
       <button type="button" className="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true">&times;</span>
       </button>
     </div>
     <div className="modal-body">
     <div className="container">
   <div className="row">
       <div className="col-md-3 col-md-4"><b>{this.state.data.desc_group} </b>- nivel {this.state.data.level}</div>
       <div className="col-md-3 col-md-4">  <button style={{backgroundColor:"#e84d66",
                              color: "#fff"}}
                   text-align="center" 
                   type="submit" 
                   className="btn btn-lg bottom-right" onClick={this.submitHandler} data-dismiss="modal"> Aceptar 	</button> </div>
       <div className="col-md-3 col-md-4">  <button style={{backgroundColor:"#e84d66",
                              color: "#fff"}}
                   text-align="center" 
                   type="submit" 
                   className="btn btn-lg bottom-right" data-dismiss="modal">Cancelar	</button> </div>
   </div>
</div>
     </div>
   </div>
 </div>
</div>
   </div>
  );
  }
}