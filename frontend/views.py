from django.shortcuts import render, redirect
from django.contrib.auth import logout

def index(request):
	return render(request, 'frontend/index.html')

def indexpk(request,pk):
	return render(request, 'frontend/index.html')

def log_out(request):
	if request.user.is_authenticated:
		logout(request)
		return redirect('login')
	return redirect('login')

def auth(request):
	if not request.user.is_authenticated:
		return render(request, 'frontend/index.html')
	return redirect('dashboard')
