from django.urls import path
from . import views

urlpatterns = [
    path('language-List/', views.languageList, name="language-List"),
    path('language-Detail/<str:pk>/', views.languageDetail, name="language-Detail"),
    path('language-Create/', views.languageCreate, name="langueage-Create"),
]
