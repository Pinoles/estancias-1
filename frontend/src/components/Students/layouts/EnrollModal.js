import React from 'react';

export default function StudentModal(props){
    return(
        <div className="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
            <div className="modal-content">
                <div className="modal-header pb-3" style={{border:"0"}}>
                    <div className="d-flex pt-3">
                        <h4><b>Informacion</b></h4>
                    </div>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body">
                    {props.data.user_id?
                        <div className="container pt-4" style={{backgroundColor:"#EEEEEE"}}>
                            <div className="row pt-4 pb-2">
                                <div className="col border-bottom">
                                    <label ><b>Nombre</b></label>
                                </div>
                                <div className="col border-bottom">
                                    <label><b>{props.data.user_id.name|| ""}</b></label>
                                </div>
                            </div>
                            <div className="row pt-2 pb-2">
                                <div className="col border-bottom">
                                    <label ><b>Correo Electronico</b></label>
                                </div>
                                <div className="col border-bottom">
                                    <label><b>{props.data.user_id.email|| ""}</b></label>
                                </div>
                            </div>
                            <div className="row pt-2 pb-2">
                                <div className="col border-bottom">
                                    <label ><b>Numero Telefonico</b></label>
                                </div>
                                <div className="col border-bottom">
                                    <label><b>{props.data.user_id.telephone || ""}</b></label>
                                </div>
                            </div>
                            <div className="row pt-2 pb-2">
                                <div className="col border-bottom">
                                    <label ><b>Curso</b></label>
                                </div>
                                <div className="col border-bottom">
                                    <label><b>{props.data.group_id.desc_group|| ""}</b></label>
                                </div>
                            </div>
                            <div className="row pt-2 pb-2">
                                <div className="col border-bottom">
                                    <label ><b>Nivel</b></label>
                                </div>
                                <div className="col border-bottom">
                                    <label><b>{props.data.group_id.level|| ""}</b></label>
                                </div>
                            </div>
                        </div>
                    :null
                    }
                </div> 
            </div>
            
        </div>
    );
}
