from django.urls import path
from  . import views

urlpatterns = [
    path('conekta', views.PaymentConekta, name='conekta' ),
]