from rest_framework import serializers
from .models import StudentsEnrollment, Students
from administration.serializers import GroupsSerializer

class GroupsListingField(serializers.RelatedField):
    def to_representation(self, value):
        group_id = GroupsSerializer(value,many=False).data
        return group_id

class UserListingField(serializers.RelatedField):
    def to_representation(self, value):
        user_id = {'name':value.__str__(),'email':value.email,'telephone':value.telephone,'birthdate':value.birthdate}
        return user_id

class StudentsEnrollmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentsEnrollment
        fields = ('__all__')

class GETStudentsEnrollmentSerializer(serializers.ModelSerializer):
    user_id = UserListingField(many=False,read_only=True)
    group_id = GroupsListingField(many=False, read_only=True)
    class Meta:
        model = StudentsEnrollment
        fields = ('__all__')

class StudentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Students
        filds = ('_all_')