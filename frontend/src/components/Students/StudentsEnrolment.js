import React, { Component } from "react";
import StudentModal from "./layouts/EnrollModal";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useParams
} from "react-router-dom";

export default class StudentsEnrollment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      group_data:[],
      language_data:[],
      viewStudent:{},
      loaded: false,
      placeholder: "Loading"
    };
    this.viewStudent = this.viewStudent.bind(this);
  }

  viewStudent(student){
    let studentjs = JSON.parse(JSON.stringify(student));
    this.setState({viewStudent:studentjs});
  }

   componentDidMount() {
     fetch("/api/Student/StudentsEnrollment-list/")
      .then(response => {
        if (response.status > 400) {
          this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            data,
            loaded: true
          };
        });
      });

      fetch("/api/Lenguages/language-List/")
          .then(response => {
            if (response.status > 400) {
              this.setState(() => {
                return { placeholder: "Something went wrong!" };
              });
            }
            return response.json();
          })
          .then(language_data => {
            this.setState(() => {
              return {
                language_data,
                loaded: true
              };
            });
          });

      fetch("/api/admin/group-list/")
      .then(response => {
        if (response.status > 400) {
          this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(group_data => {
        this.setState(() => {
          return {
            group_data,
            loaded: true
          };
        });
      });
  }

  render() {
    return (
      <div className="row justify-content-center">
        <div className="modal fade m-auto" id="exampleModalScrollable" tabIndex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
         <StudentModal 
            data={this.state.viewStudent}
         />
         </div>
        <div className="col-10 card mt-3 shadow" >
          <div className="card-body">
            <h5>Gestionar Alumnos</h5>
          </div>
        </div>
        <div className="col-10 mt-3" style={{backgroundColor:"#fff"}}>
          <Router>
            <Switch>
              <Route exact path="/estudiantesEnrrolados/">
                <Menu></Menu>
              </Route>
              <Route exact path="/estudiantesEnrrolados/Todos/">
                <Todos data={this.state.data} viewStudent={this.viewStudent}></Todos>
              </Route>
              <Route exact path="/estudiantesEnrrolados/Grupos/">
                <Grupos data={this.state.group_data}></Grupos>
              </Route>
              <Route exact path="/estudiantesEnrrolados/Grupos/:id/">
                <Gruposid data={this.state.data} viewStudent={this.viewStudent}></Gruposid>
              </Route>
              <Route exact path="/estudiantesEnrrolados/Lenguajes/">
                <Lenguajes data={this.state.language_data}></Lenguajes>
              </Route>
              <Route exact path="/estudiantesEnrrolados/Lenguajes/:id/">
                <Lenguajesid data={this.state.data} viewStudent={this.viewStudent}></Lenguajesid>
              </Route>
              <Route exact path="/estudiantesEnrrolados/Modalidades/">
                <Modalidades></Modalidades>
              </Route>
              <Route exact path="/estudiantesEnrrolados/Modalidades/:id/">
                <Modalidadesid data={this.state.data} viewStudent={this.viewStudent}></Modalidadesid>
              </Route>
            </Switch>
          </Router>
        </div>
      </div>
    );
  }
}

function Menu() {
  return (
  <div className="row no-gutters pt-4 pb-4">
    <div className="col-12 card card-header shadow" style={{padding:"0"}}>
        <Link to="./Todos/" className="ml-3 mb-1 mt-1">
          <button className="btn btn-link btn-block text-left">
            <b>Todos</b>
          </button>
        </Link>
    </div>
    <div className="col-12 card card-header mt-4 shadow" style={{padding:"0"}}>
      <Link to="./Grupos/" className="ml-3 mb-1 mt-1">
        <button className="btn btn-link btn-block text-left">
          <b>Grupo</b>
        </button>
      </Link>
    </div>
    <div className="col-12 card card-header mt-4 shadow" style={{padding:"0"}}>
      <Link to="./Lenguajes/" className="ml-3 mb-1 mt-1">
        <button className="btn btn-link btn-block text-left">
          <b>Lenguajes</b>
        </button>
      </Link>
    </div>
    <div className="col-12 card card-header mt-4 shadow" style={{padding:"0"}}>
      <Link to='./Modalidades/' className="ml-3 mb-1 mt-1">
        <button className="btn btn-link btn-block text-left">
          <b>Modalidad</b>
        </button>
      </Link>
    </div>
  </div>
  );
}

function Todos(props) {
  return (
    <div className="row no-gutters pt-4 pb-4">
      <h5 className="mb-0">Todos los alumnos</h5>
      {props.data.map(StudentsEnrollment => {
        return(
          <div className="col-12 card card-header mt-4 " style={{padding:"0", border:"0"}} key={StudentsEnrollment.students_enroll_id}>
            <p className="ml-3 mb-1 mt-1">
              <button className="btn btn-link btn-block text-left" onClick={() => props.viewStudent(StudentsEnrollment)} data-toggle="modal" data-target="#exampleModalScrollable">
                <b>{StudentsEnrollment.user_id['name']}</b>
              </button>
            </p>
          </div>
        );
      })}
    </div>
  );
}

function Grupos(props){
  return (
  <div className="row no-gutters pt-4 pb-4">
    <h5 className="mb-0"></h5>
    {props.data.map(groups => {
      return(
        <div className="col-12 card card-header shadow mt-4 " style={{padding:"0"}} key={groups.group_id}>
          <Link to={"./"+groups.group_id +"/"} className="ml-3 mb-1 mt-1">
          <button className="btn btn-link btn-block text-left">
            {groups.desc_group}
            </button>
          </Link>
        </div>
      );
    })}
  </div>
  );
}

function Gruposid(props){
  let id = useParams();
  let inscritos = [];
  inscritos = props.data.filter(function (n){
    return n.group_id.group_id == id.id;
  });
  console.log(inscritos);
  return (
  <Todos data = {inscritos} viewStudent={props.viewStudent}>

  </Todos>
  );
}

function Lenguajes(props){
  return (
    <div className="row no-gutters pt-4 pb-4">
      <h5 className="mb-0"></h5>
      {props.data.map(language => {
        return(
          <div className="col-12 card card-header shadow mt-4 " style={{padding:"0"}} key={language.lenguage_id}>
            <Link to={"./"+language.lenguage_id +"/"} className="ml-3 mb-1 mt-1">
            <button className="btn btn-link btn-block text-left">
              {language.desc_lenguage}
              </button>
            </Link>
          </div>
        );
      })}
    </div>
    );
}

function Lenguajesid(props){
  let id = useParams();
  let inscritos = [];
  inscritos = props.data.filter(function (n){
    return n.group_id.lenguage_id == id.id;
  });
  console.log(inscritos);
  return (
  <Todos data = {inscritos} viewStudent={props.viewStudent}/>
  );
}

function Modalidades(){
  return (
    <div className="row no-gutters pt-4 pb-4">
      <h5 className="mb-0"></h5>
      <div className="col-12 card card-header shadow mt-4 " style={{padding:"0"}}>
        <Link to={"./M1/"} className="ml-3 mb-1 mt-1">
        <button className="btn btn-link btn-block text-left">
          Semanal
          </button>
        </Link>
      </div>
      <div className="col-12 card card-header shadow mt-4 " style={{padding:"0"}}>
        <Link to={"./M2/"} className="ml-3 mb-1 mt-1">
        <button className="btn btn-link btn-block text-left">
          Sabatino
          </button>
        </Link>
      </div>
      <div className="col-12 card card-header shadow mt-4 " style={{padding:"0"}}>
        <Link to={"./M3/"} className="ml-3 mb-1 mt-1">
        <button className="btn btn-link btn-block text-left">
          Kids
          </button>
        </Link>
      </div>
    </div>
    );
}

function Modalidadesid(props){
  let id = useParams();
  let inscritos = [];
  inscritos = props.data.filter(function (n){
    return n.group_id.course_type == id.id;
  });
  console.log(inscritos);
  return (
  <Todos data = {inscritos} viewStudent={props.viewStudent}/>
  );
}