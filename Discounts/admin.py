from django.contrib import admin
from .models import Discounts, DiscountTypes
# Register your models here.
modelos = [DiscountTypes, Discounts]
admin.site.register(modelos)
