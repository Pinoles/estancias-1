import React, { Component } from 'react'

export default class cycleCreate extends Component{
    constructor(props){
        super(props);
        this.state = {
            desc_cycle:"",
            start_date:"2000-01-01",
            end_date:"2000-01-01",
            allow_change:false,
        };

        this.changeHandler = this.changeHandler.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
        this.changeHandlerCB = this.changeHandlerCB.bind(this);
    }

    changeHandler(e) {
        this.setState({[e.target.name]: e.target.value});
    }
    changeHandlerCB(e) {
        this.setState({[e.target.name]: e.target.checked});
    }

    submitHandler(e){
        e.preventDefault();
        console.log(this.state);
        const csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        fetch("/api/Cycles/cycles-Create/", {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(this.state), // data can be `string` or {object}!
            headers:{
                'Content-Type': 'application/json',
                'X-CSRFToken':csrf_token
            },
            }).then(res => {
            if (res.status > 400) {
                this.setState(() => {
                return { placeholder: "Something went wrong!" };
                });
            }
            return res.json();
            })
            .catch(error => console.error('Error:', error))
            .then(response =>
            {if(!response.errors){
                this.props.history.goBack();
                }else{
                console.log(response);
                }}
            );
    }

    render(){
        const {desc_cycle,start_date,end_date,allow_change} = this.state;

        return(
            <div>
               
               <form style={{marginTop: "80px",marginBottom: "80px"}} className="card text-center" onSubmit={this.submitHandler} >

            <div className="card text-center card bg-light col-md-8" style={{maxWidth: "680px",marginLeft: "340px",backgroundColor:"#EEEEEE"}}>
                <h5 className="card-header">Registrar Ciclo</h5>
                <div   style={{maxWidth: "680px"}} className="card-body">
                    <div  className="form-group row">
                        <label htmlFor="inputName" className="col-sm-3 col-form-label">Descripción</label>
                        <div className="col-sm-9">
                        <input type="text" 
                            className="form-control" 
                            id="inputName" name="desc_cycle" 
                            placeholder="Descripción de ciclo" 
                            value={desc_cycle}
                            onChange={this.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputBirthday" className="col-sm-3 col-form-label">Dia de inicio</label>
                        <div className="col-sm-1">
                            <input type="date" id="start" name="start_date"
                            value={start_date}
                            min="1960-01-01" max="2040-12-31"
                            onChange={this.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputBirthday" className="col-sm-3 col-form-label">Dia de fin</label>
                        <div className="col-sm-1">
                            <input type="date" id="start" name="end_date"
                            value={end_date}
                            min="1960-01-01" max="2040-12-31"
                            onChange={this.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="checkchange" className="col-sm-3 col-form-label">Permitir Cambios</label>
                        <div className="col-sm-1">
                            <input type="checkbox" id="checkchange" name="allow_change" checked={allow_change} onChange={this.changeHandlerCB}/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <div className="col-sm-12 text-center">
                        <button text-align="center" type="submit" className="btn btn-outline-danger btn-lg center-block">Registrar</button>
                        </div>
                    </div>
                </div>
            </div>
            </form> 
            </div>
        );
    }
}