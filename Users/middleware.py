from django.utils.deprecation import MiddlewareMixin
from django.urls import resolve, reverse
from django.http import HttpResponseRedirect
from django.conf import settings
from django.urls import get_resolver

class LoginRequiredMiddleware(MiddlewareMixin):
    def process_request(self, request):
        assert hasattr(request, 'user')
        routesExcept = ('login','register', 'userAuth','userCreate','userLoget')
        routesAdminExcept = ('dashboard','profile','logout','userDetail','userUpdate',
            'userLoget','studentGroups','group-List','preRegister-Create','preRegister-Detail',
            'studentPreGroups','StudentsEnrollment-Create','group-Detail')
        # variable para declarar más rutas validas sin login
        #routesall = routesExcept + tuple(routesPrincipal) # agregar y sumar rutas validas y rutas declaradas
        #si no está auntenticado
        current_route_name = resolve(request.path_info).url_name

        if not request.user.is_authenticated:
            if not current_route_name in routesExcept:
            	#regresar a ruta de login
                return HttpResponseRedirect(reverse(settings.AUTH_LOGIN_ROUTE))
        elif not request.user.is_admin():
            if not current_route_name in routesAdminExcept:
                return HttpResponseRedirect(reverse('dashboard'))
