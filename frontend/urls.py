from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='dashboard' ),
    path('registro/',views.auth, name='register'),
    path('login/',views.auth, name='login'),
    path('grupos/',views.index, name='grupos'),
    path('grupos/<str:pk>/',views.indexpk, name='gruposSh'),
    path('grupos/<str:pk>/nuevo/',views.indexpk, name='gruposShNew'),
    path('logout/',views.log_out, name='logout'),
    path('profile/',views.index, name='profile'),
    path('usuarios/',views.index, name='usuarioslist'),
    path('usuarios/nuevo/',views.index, name='usuariosNew'),
    path('ciclos/',views.index, name="cycles"),
    path('ciclos/nuevo/',views.index, name="cyclesNew"),
    path('profesores/',views.index, name="teachers"),
    path('profesores/nuevo/',views.index, name="teachersNew"),
    path('lenguajes/',views.index, name="languages"),
    path('lenguajes/nuevo/',views.index, name="langagesNew"),
    path('estudiante/grupos/',views.index,name="studentGroups"),
    path('estudiante/grupos/<str:pk>/',views.indexpk,name="studentGroups"),
    path('estudiante/gruposPregistrado/<str:pk>/',views.indexpk,name="studentPreGroups"),
    path('estudiantesEnrrolados/',views.index,name="studentEnroll"),
    path('estudiantesEnrrolados/Todos/',views.index,name="studentsEnrollList"),
    path('estudiantesEnrrolados/Grupos/', views.index, name="studentsGroup"),
    path('estudiantesEnrrolados/Grupos/<str:pk>/', views.indexpk,name="studentsGroupDetail"),
    path('estudiantesEnrrolados/Lenguajes/',views.index,name="studentsEnrollLanguage"),
    path('estudiantesEnrrolados/Lenguajes/<str:pk>/',views.indexpk,name="studentsEnrollLanguageDetail"),
    path('estudiantesEnrrolados/Modalidades/',views.index,name="studentsEnrollModal"),
    path('estudiantesEnrrolados/Lenguajes/<str:pk>/',views.indexpk,name="studentsEnrollModalDetail"),
    path('pagoExamen/',views.index,name="pagoExamen"),
    path('pago/tarjeta/',views.index, name="pagoTarjeta")
]