from django.urls import path
from . import views

urlpatterns = [
    path('teacher-list/', views.teacherList, name="teacher-List"),
    path('teacher-Detail/<str:pk>/', views.teacherDetail, name="teacher-Detail"),
    path('teacher-Create/', views.teacherCreate, name="teacher-Create"),
]