from django.db import models
# Create your models here.
dict_error_messages = {'max_length':'muy largo','blank':'no se puede dejar en blanco'}

class DiscountTypes(models.Model):
    discount_type_id=models.AutoField(auto_created=True,primary_key=True)
    desc_discount_type=models.CharField('tipo de descuento',max_length=30,error_messages=dict_error_messages)

class Discounts(models.Model):
    discount_id=models.AutoField(auto_created=True,primary_key=True)
    created_at=models.DateField(auto_now_add=True)
    autorized_by=models.ForeignKey('Users.Users',on_delete=models.SET_NULL, null = True)
    ammount=models.DecimalField('monto de descuento',decimal_places=3,max_digits=5)
    discount_type_id=models.ForeignKey('DiscountTypes',on_delete=models.SET_NULL, null = True)
    cycle_id=models.ForeignKey('Cycles.Cycles',on_delete=models.SET_NULL, null = True)
    student_id=models.ForeignKey('Students.Students',on_delete=models.SET_NULL, null = True)