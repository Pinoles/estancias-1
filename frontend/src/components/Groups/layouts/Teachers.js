import React, { Component } from 'react'

export default class Teachers extends Component{
    constructor(props) {
        super(props);
        this.state = {
          data: [],
          loaded: false,
          placeholder: "Loading"
        };
      }
    
      componentDidMount() {
        fetch("/api/Teachers/teacher-list/")
          .then(response => {
            if (response.status > 400) {
              return this.setState(() => {
                return { placeholder: "Something went wrong!" };
              });
            }
            return response.json();
          })
          .then(data => {
            this.setState(() => {
              return {
                data,
                loaded: true
              };
            });
          });
      }
    render(){
        return(this.state.data.map(teacher => {
                return (
                    <option value={teacher.teacher_id}>{teacher.user_id}</option>
                );
                })
        );
    }
}
