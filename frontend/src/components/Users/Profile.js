import React, { Component } from 'react'
import Userdata from './layouts/userdata';

export default class profile extends Component{
  constructor(props) {
    super(props);
    this.state = {
      data: {id:"",name:"",lastname:"",email:"",telephone:"",birthdate:""},
      predata:{id:"",name:"",lastname:"",email:"",telephone:"",birthdate:""},
      edit:false,
      loaded: false,
      placeholder: "Loading"
    };

    this.changeEdit = this.changeEdit.bind(this);
    this.changeHandler = this.changeHandler.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
    this.changeCancel = this.changeCancel.bind(this);
  }
  
  componentDidMount() {
    fetch("/api/Users/userDetailAuth")
    .then(response => {
      if (response.status > 400) {
        return this.setState(() => {
          return { placeholder: "Something went wrong!" };
        });
      }
      return response.json();
    })
    .then(data => {
      this.setState(() => {
        return {
          data,
          loaded: true
        };
      });
    });
  }

  changeHandler(e) {
    let newData = this.state.data;
    newData[e.target.name] = e.target.value;
    this.setState({data:newData});
  }

  submitHandler(e){
    e.preventDefault();
    //
    let newData = this.state.data;
    const csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    fetch("/api/Users/userUpdate/"+newData['id']+"/", {
        method: 'PUT', // or 'PUT'
        body: JSON.stringify(this.state.data), // data can be `string` or {object}!
        headers:{
          'Content-Type': 'application/json',
          'X-CSRFToken':csrf_token
        },
        
      }).then(res => {
        if (res.status > 400) {
          return this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return res.json();
      })
      .catch(error => console.error('Error:', error))
      .then(response =>
        {if(!response.errors){
          this.setState({edit:false,data:response['user'],predata:response['user']});
          console.log(response);
            //window.location.href = '../';
          }else{
            console.log(response);
          }}
      );
    
}

changeEdit(){
  this.setState(() => {
    const predata = JSON.parse(JSON.stringify(this.state.data));
    return {
      predata,
      edit: true
    };
  });
  console.log(this.state);
}

changeCancel(){
  this.setState(() => {
    const data = JSON.parse(JSON.stringify(this.state.predata));
    return {
      data,
      edit: false
    };
  });
  console.log(this.state);
}
  render(){
      return (
        <div>
          
          <Userdata handleChange={this.changeHandler}
              data={this.state.data} 
              submitHandler={this.submitHandler} 
              editable={this.state.edit}
              cancel={this.changeCancel}
              changeEdit ={this.changeEdit}>
          </Userdata>
                 </div>  
                );
    }
}
