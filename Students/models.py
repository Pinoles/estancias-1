from django.db import models
# Create your models here.
dict_error_messages = {'max_length':'muy largo','blank':'no se puede dejar en blanco'}

class Students(models.Model):
    student_id = models.AutoField(auto_created=True,primary_key=True)
    user_id = models.OneToOneField('Users.Users',on_delete=models.SET_NULL,null=True)
    UJED_key = models.CharField('Llave de la UJED para alumnos',unique=True,max_length=10,error_messages=dict_error_messages)
    def __str__(self):
        full_name = '%s' % (self.user_id)
        return full_name.strip()

class StudentsEnrollment(models.Model):
    students_enroll_id = models.AutoField(auto_created=True,primary_key=True)
    user_id = models.ForeignKey('Users.Users',on_delete=models.SET_NULL, null = True)
    group_id = models.ForeignKey('administration.Groups',on_delete=models.SET_NULL, null = True)
    enrollment_date = models.DateField('Fecha de enrolamiento', auto_now_add=True)
    payment_reference = models.ForeignKey('Payments.Payments',on_delete=models.SET_NULL, null = True,to_field='payment_reference')

    class Meta:
        unique_together=('user_id','group_id')
