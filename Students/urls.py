from django.urls import path
from . import views

urlpatterns = [
    path('Students-list/', views.StudentsList, name="Students-List"),
    path('Students-Detail/<str:pk>/', views.StudentsDetail, name="Students-Detail"),
    path('Students-Create/', views.StudentsCreate, name="Students-Create"),
    path('StudentsEnrollment-list/', views.StudentsEnrollmentList, name="StudentsEnrollment-List"),
    path('StudentsEnrollment-Detail/<str:pk>/', views.StudentsEnrollmentDetail, name="StudentsEnrollment-Detail"),
    path('StudentsEnrollment-Create/', views.StudentsEnrollmentCreate, name="StudentsEnrollment-Create"),
]
