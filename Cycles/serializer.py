from rest_framework import serializers
from .models import Cycles

class CyclesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cycles
        fields = ('__all__')