from rest_framework import serializers
from .models import Groups, GroupsSchedule, PreRegisterGroup

class GroupsListingField(serializers.RelatedField):
    def to_representation(self, value):
        group_id = GetGroupsSerializer(value,many=False).data
        return group_id

class GetGroupsSerializer(serializers.ModelSerializer):
    preregister=serializers.StringRelatedField(many=True)
    groupSh=serializers.StringRelatedField(many=True)
    lenguage_id= serializers.SlugRelatedField(
    many=False,
    read_only=True,
    slug_field='desc_lenguage'
    )
    cycle_id = serializers.SlugRelatedField(
        many=False,
        read_only=True,
        slug_field='desc_cycle'
    )
    teacher_id = serializers.StringRelatedField(
        many=False
    )
    course_type = serializers.SerializerMethodField()

    class Meta:
        model = Groups 
        fields = ('__all__')
    
    def get_course_type(self,obj):
        return obj.get_course_type_display()

class GroupsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Groups 
        fields = ('__all__')

class GroupsScheduleSerializer(serializers.ModelSerializer):
    day_of_week1 = serializers.SerializerMethodField()

    class Meta:
        model = GroupsSchedule
        fields = ('__all__')

    def get_day_of_week1(self,obj):
        return obj.get_day_of_week_display()

class GETPreRegisterGroupSerializers(serializers.ModelSerializer):
    group_id = GroupsListingField(read_only=True, many=False)
    user_id = serializers.StringRelatedField()
    class Meta:
        model = PreRegisterGroup
        fields= ('__all__')

class PreRegisterGroupSerializers(serializers.ModelSerializer):
    class Meta:
        model = PreRegisterGroup
        fields= ('__all__')