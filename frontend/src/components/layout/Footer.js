import React from 'react';

export default function Footer(props){
      
        return (
          <footer  className="page-footer font-small white pt-4" style={{backgroundColor:"#355f8d",height:"200px"}}>
      
      <div className="container-fluid text-center text-md-left">
        <div className="row">
       
         <div className="col-md-3 mb-md-0 mb-2">

            <ul   style={{textAlign:"right"}} className="list-unstyled">
              <li>
                <a style={{color:"#edeef0"}}href="#!">Inicio</a>
              </li>
              <li>
                <a style={{color:"#edeef0"}}href="#!">Descubrir</a>
              </li>
              <li>
                <a style={{color:"#edeef0"}}href="#!">Fotos</a>
              </li>
              <li>
                <a style={{color:"#edeef0"}}href="#!">Contacto</a>
              </li>
            </ul>
          </div>
          <div className="col-md-3 mb-md-0 mb-2">
            <ul   style={{textAlign:"right"}} className="list-unstyled">
              <li>
                <a style={{color:"#edeef0"}} href="#!">Sobre nosotros</a>
              </li>
              <li>
                <a style={{color:"#edeef0"}}href="#!">Ayuda</a>
              </li>
              <li>
                <a  style={{color:"#edeef0"}}href="#!">Terminos</a>
              </li>
              <li>
                <a  style={{color:"#edeef0"}}href="#!">Guia</a>
              </li>
            </ul>
          </div>
        
          <div className="col-md-3 mb-md-0 mb-2">
           
            <ul style={{textAlign:"right"}} className="list-unstyled">
              <li>
                <a style={{color:"#edeef0"}}href="#!">Testimonios</a>
              </li>
              <li>
                <a style={{color:"#edeef0"}} href="#!">Anuncios</a>
              </li>
              <li>
                <a style={{color:"#edeef0"}} href="#!">Integracíon</a>
              </li>
              <li>
                <a style={{color:"#edeef0"}}href="#!">Carreras</a>
              </li>
            </ul>
          </div>
          <div className="col-md-3 mb-md-0 mb-3">
            <form className="form-inline">
               <input className="form-control form-control-sm mr-3 w-75" type="text" placeholder="Email"
                 aria-label="Buscar"></input>
               <i className="fas fa-search" aria-hidden="true"></i>
             </form>
          </div>
        </div>
      </div>
 <div>           
<div style={{padding:"0"}}  alig="center" className="p-3 mb-2 bg-light text-dark">

  <div style={{textAlign:"webkit-center"}} className="container">
   <div className="row">
     <div style={{textAlign:"webkit-center",paddingTop:"25px",textAlign:"center"}} className="col-sm">
      <img style={{width:"25px"}} src="https://webstockreview.net/images600_/red-twitter-png-1.png" alt=""></img>
     </div>
     <div style={{textAlign:"webkit-center",paddingTop:"25px",textAlign:"center"}}className="col-sm">
     
         <img style={{width:"25px",filter:"saturate(0.5)"}}src="https://rhpositivo.net/wp-content/uploads/2019/04/instagram-2-icon-14-256.png" alt=""></img>
        
     </div>
     <div style={{textAlign:"webkit-center"}}  className="col-sm">
     <div style={{textAlign:"webkit-left"}}className="col-sm">
      <img style={{width:"100%"}}src="http://fcqgp.ujed.mx/img/logo-01.png" className="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" alt=""></img>
     </div>
     </div>
     <div style={{textAlign:"webkit-center",paddingTop:"25px",textAlign:"center"}} className="col-sm">
     
         <img style={{width:"25px"}}src="https://i.ya-webdesign.com/images/red-facebook-png-2.png"  alt=""></img>
       
    </div>
    <div style={{textAlign:"webkit-center",textAlign:"center",paddingTop:"20px"}} className="col-sm">
     
      <i style={{color:"#B11830",fontSize:"30px"}} className="icon ion-md-globe"></i>
        
    </div>
   </div>
 </div>


</div>

</div>    
<div  style={{padding:"0"}} alig="center" className="p-3 mb-2 bg-danger text-white">
</div>
        </footer>           
        );
}
