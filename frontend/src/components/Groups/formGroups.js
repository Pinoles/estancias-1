import React, { Component } from 'react'
import Groups from './layouts/groups'
import Teachers from './layouts/Teachers'
import Lengauges from './layouts/Lenguages'
import Cycles from './layouts/Cycles'

export default class formGroups extends Component{
  constructor(props){
      super(props);
      this.state = {
          desc_group:"",
          lenguage_id:"",
          cycle_id:"",
          max_capacity:"",
          level:"",
          room:"",
          teacher_id:"",
          course_type:"M2"
      };

      this.changeHandler = this.changeHandler.bind(this);
      this.submitHandler = this.submitHandler.bind(this);
  }

  changeHandler(e) {
      this.setState({[e.target.name]: e.target.value});
  }

  submitHandler(e){
    e.preventDefault();
    console.log(this.state);
    const csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    fetch("/api/admin/group-Create/", {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(this.state), // data can be `string` or {object}!
        headers:{
            'Content-Type': 'application/json',
            'X-CSRFToken':csrf_token
        }
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => window.location.reload(true));
  }

  render(){
    const {desc_group,lenguage_id,cycle_id,max_capacity,level,room,teacher_id,course_type} = this.state;
    return(
      <div className="row justify-content-center">
        <div className="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
          <div className="modal-dialog modal-dialog-scrollable" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalScrollableTitle">Registro grupos</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form>
                  <div className="row">
                    <div className="col">
                      <input type="text" className="form-control" id="desc_group" placeholder="Descripcion" name="desc_group" value={desc_group} onChange={this.changeHandler}/>
                    </div>
                  </div>
                  <h1></h1>
                  <div className="row">
                    <div className="col">
                      <select className="form-control" value={lenguage_id} name="lenguage_id" onChange={this.changeHandler}>
                        <option value="" selected>-Seleccione el lenguaje-</option>
                        <Lengauges></Lengauges>
                      </select>
                    </div>
                  </div>  
                  <h1></h1>
                  <div className="row">
                    <div className="col">
                      <select className="form-control" value={cycle_id} name="cycle_id" onChange={this.changeHandler}>
                        <option value="" selected>-Seleccione el ciclo-</option>
                        <Cycles></Cycles>
                      </select>
                   </div>
                  </div>
                  <h1></h1>
                  <div className="row">
                    <div className="col">
                      <input type="number" className="form-control" id="max_capacity"placeholder="Capacidad maxima del grupo" name="max_capacity" value={max_capacity} onChange={this.changeHandler} />
                    </div>
                  </div>      
                  <h1></h1>
                  <div className="row">
                    <div className="col">
                      <input type="text" className="form-control" id="level"placeholder="Nivel del grupo" name="level" value={level} onChange={this.changeHandler}/>   
                    </div>
                  </div> 
                  <h1></h1>
                  <div className="row">
                    <div className="col">
                      <input type="text" className="form-control" id="Room"placeholder="Salón" name="room" value={room} onChange={this.changeHandler}/>
                    </div>
                  </div>  
                  <h1></h1>
                  <div className="row">
                    <div className="col">
                      <select className="form-control" value={teacher_id} name="teacher_id" onChange={this.changeHandler}>
                        <option value="" selected>-Seleccione el profesor-</option>
                        <Teachers></Teachers>
                      </select>
                    </div>
                  </div>  
                  <h1></h1>
                  <div className="row">     
                    <div className="col">
                      <select className="form-control" value={course_type} name="course_type"onChange={this.changeHandler}>
                        <option value="M1">Semanal</option>
                        <option value="M2" selected>Sabatino</option>
                        <option value="M3">Kids</option>
                        <option value="M4">Auto Aprendizaje</option>
                      </select>
                    </div>
                  </div>
                      
                </form>
              </div>
              <div className="modal-footer">
                <button style={{color:"white",backgroundColor:"#961429"}} type="button" className="btn bnt" data-dismiss="modal">Cerrar</button>
                <button  style={{color:"white",backgroundColor:"#355F8D"}} type="button" className="btn btn" data-dismiss="modal" onClick={this.submitHandler}>Guardar Cambios</button>
              </div>
            </div>
          </div>
        </div>

        <div className="col-11 card mt-3" style={{border:"0"}}>
          <div className="card-body p-2">
            <h5>Administrar Grupos</h5>
          </div>
        </div>
        <div className="col-12 mt-4">
          <button  style={{color:"white",backgroundColor:"#355F8D"}} type="button" className="btn btn-sm" data-toggle="modal" data-target="#exampleModalScrollable">
            Nuevo Grupo
          </button>
        </div>
        <div className="table-responsive">
        <table className="table table-hover">
          <thead>
            <tr>
              <th scope="col"></th>
              <th scope="col">
                <p className="font-weight-normal">
                Idioma
                </p>
              </th>
              <th scope="col">
              <p className="font-weight-normal">
                Nivel
                </p>
              </th>
              <th scope="col">
              <p className="font-weight-normal">
                Cupo
              </p>
              </th>
              <th scope="col">
              <p className="font-weight-normal">
                Tipo de curso
                </p>
              </th>
              <th scope="col">
              <p className="font-weight-normal">
                Horario
                </p>
              </th>
              <th scope="col">
              <p className="font-weight-normal">
                Aula
                </p>
              </th>
              <th scope="col"></th>
            </tr>
          </thead>
            <Groups></Groups>
        </table>
        </div>
      </div>
    );
  }
}