from django.db import models
# Create your models here.
class Teachers(models.Model):
    teacher_id=models.AutoField(auto_created=True,primary_key=True)
    user_id=models.OneToOneField('Users.Users',on_delete=models.SET_NULL, null = True)

    def __str__(self):
        return str(self.user_id)