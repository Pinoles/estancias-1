from django.urls import path
from . import views

urlpatterns = [
    path('', views.apiOverview ,name='apiOverview'),
    path('userList/',views.userList,name='userList'),
    path('userDetail/<str:pk>/',views.userDetail,name='userDetailpk'),
    path('userDetailAuth/',views.userDetailAuth,name='userDetail'),
    path('userCreate/',views.userCreate,name='userCreate'),
    path('userUpdate/<str:pk>/',views.userUpdate,name='userUpdate'),
    path('userDelete/<str:pk>/',views.userDelete,name='userDelete'),
    path('userVerificate/<str:pk>/',views.userVerificate,name='userVerificate'),
    path('userAuth/',views.userAuth,name='userAuth'),
    path('userLogout/',views.userLogout,name='userLogout'),
    path('userLoget/',views.userLoget,name='userLoget'),
    path('userTeacher/', views.userTeacher,name='userTeacher')
    ]
