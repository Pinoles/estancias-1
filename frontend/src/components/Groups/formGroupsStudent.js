import React, { Component } from "react";
import GroupModal from './layouts/GroupModal';
import { Link } from "react-router-dom";

export default class studentformGroups extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loaded: false,
      placeholder: "Loading",
    };
  }

  componentDidMount() {
    fetch("/api/admin/group-list/")
      .then(response => {
        if (response.status > 400) {
           this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            data,
            loaded: true
          };
        });
      });

      fetch("/api/Users/userLoget")
      .then(response => {
        if (response.status > 400) {
          this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        const pregister = {
          group_id:"",
          user_id:data.id
          };
        this.setState(() => {
          return {
            pregister,
            loaded: true
          };
        });
      });
  }

  render() {
    console.log(this.state);
    return (
      <div>
        
        <h1>Grupos disponibles</h1>
        <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Descripción</th>
            <th scope="col">Lenguaje</th>
            <th scope="col">Curso</th>
            <th scope="col">Capacidad</th>
            <th scope="col">Aula</th>
            <th scope="col">Ciclo</th>
            <th scope="col">Nivel</th>
            <th scope="col">Maestro</th>
            <th scope="col">Acciones</th>
          </tr>
        </thead>
            <tbody>
            {this.state.data.map(group => {
                return (
                    <tr key={group.group_id}>
                        <th scope="row">{group.group_id}</th>
                        <th scope="row">{group.desc_group}</th>
                        <th scope="row">{group.lenguage_id}</th>
                        <th scope="row">{group.course_type}</th>
                        <th scope="row">{group.max_capacity}</th>
                        <th scope="row">{group.room}</th>
                        <th scope="row">{group.cycle_id}</th>
                        <th scope="row">{group.level}</th>
                        <th scope="row">{group.teacher_id}</th>
                        <th scope="row">
                        <Link to={"./"+group.group_id+"/"}>
                          <button style={{color:"white",backgroundColor:"#355F8D"}} className="btn btn">
                            Ver Curso
                          </button>
                        </Link>
                        </th>
                    </tr>
                );
                })}
            </tbody>
        </table>
      </div>
    );
  }
}
