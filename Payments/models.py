from django.db import models
from datetime import datetime
# Create your models here.
dict_error_messages = {'max_length':'muy largo','blank':'no se puede dejar en blanco'}

class PaymentMethods(models.Model):
    payment_method_id = models.AutoField(auto_created=True,primary_key=True)
    desc_method = models.CharField('descripcion de metodo',max_length=20,error_messages=dict_error_messages)
    
class Payments(models.Model):
    payment_id=models.AutoField(auto_created=True,primary_key=True)
    payment_date=models.DateField(default=datetime.now)
    payment_reference=models.CharField('referencia de pago',max_length=32,unique=True,error_messages=dict_error_messages)
    ammount=models.DecimalField('monto a pagar',max_digits=7,decimal_places=2)