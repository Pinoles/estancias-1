import React, { Component } from "react";
import GroupModal from './layouts/GroupModal';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";


export default class studentformGroupsPre extends Component {
  constructor(props) {
    super(props);
    let id = this.props.match.params.id;
    this.state = {
      data: [],
      viewGroup:{
        desc_group:"",
        lenguage_id:"",
        cycle_id:"",
        max_capacity:"",
        level:"",
        room:"",
        teacher_id:"",
        course_type:""
      },
      pregister:{
        group_id:"",
        user_id:parseInt(id)
        },
      loaded: false,
      placeholder: "Loading",
    };

    this.viewGroup = this.viewGroup.bind(this);
    this.submitHandler = this.submitHandler.bind(this);
  }

  viewGroup(group){
    let grupojs = JSON.parse(JSON.stringify(group));
    let pregister = this.state.pregister;
    pregister["group_id"] = group["group_id"];
    this.setState({viewGroup:grupojs,pregister});
  }

  submitHandler(e){
    e.preventDefault();
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    const csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    fetch("/api/Student/StudentsEnrollment-Create/", {
        method: 'POST', // or 'PUT'
        body: JSON.stringify(this.state.pregister), // data can be `string` or {object}!
        headers:{
          'Content-Type': 'application/json',
          'X-CSRFToken':csrf_token
        },
      }).then(response => {
        return response.json();
      }).then(res =>{
        if(!res.errors){
          var docDefinition = {
            content: [
              {
                text: ['nombre: ',res.data.user_id.name]
              },
              {
                text: ['descripcion: ',res.data.group_id.desc_group]
              },
              {
                text: ['Nivel: ',res.data.group_id.level]
              },
              {
                text: ['Modalidad: ',res.data.group_id.course_type]
              },
              {
                text: ['Fecha de Inscripcion: ',res.data.enrollment_date]
              },
              {
                text: ['Referencia de pago: ',res.data.payment_reference]
              },
            ]
          }
        }
            var win = window.open('', '_blank');
            pdfMake.createPdf(docDefinition).download();
          }
      );
      /*.then(blob => URL.createObjectURL(blob))
      .then(url => {
          window.open(url, '_blank');
          URL.revokeObjectURL(url);
      });*/
  }

  componentDidMount() {
    fetch("/api/admin/preRegister-Detail/"+this.state.pregister.user_id+"/")
      .then(response => {
        if (response.status > 400) {
          return this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            data,
            loaded: true
          };
        });
      });
  }

  render() {
    console.log(this.state);
    return (
      <div>
        <h1>Grupos disponibles</h1>
        <div className="modal fade" id="exampleModalScrollable" tabIndex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
         <GroupModal 
            data={this.state.viewGroup}
            submitHandler = {this.submitHandler}
         />
         </div>
        <table className="table table-striped">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Descripción</th>
            <th scope="col">Lenguaje</th>
            <th scope="col">Curso</th>
            <th scope="col">Aula</th>
            <th scope="col">Ciclo</th>
            <th scope="col">Nivel</th>
            <th scope="col">Maestro</th>
            <th scope="col">Acciones</th>
          </tr>
        </thead>
            <tbody>
            {this.state.data.map(group => {
                return (
                    <tr key={group.group_id}>
                        <th scope="row">{group.group_id.group_id}</th>
                        <th scope="row">{group.group_id.desc_group}</th>
                        <th scope="row">{group.group_id.lenguage_id}</th>
                        <th scope="row">{group.group_id.course_type}</th>
                        <th scope="row">{group.group_id.room}</th>
                        <th scope="row">{group.group_id.cycle_id}</th>
                        <th scope="row">{group.group_id.level}</th>
                        <th scope="row">{group.group_id.teacher_id}</th>
                        <th scope="row">
                        <button className="btn btn-success" onClick={() => this.viewGroup(group.group_id)} data-toggle="modal" data-target="#exampleModalScrollable">
                          Ver Curso PreRegistrado
                        </button>
                        </th>
                    </tr>
                );
                })}
            </tbody>
        </table>
      </div>
    );
  }
}
