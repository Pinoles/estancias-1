from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.utils.timezone import now
# Create your models here.
dict_error_messages = {'max_length':'muy largo','blank':'no se puede dejar en blanco'}

class UsersManager(BaseUserManager):
    def create_user(self, email, password = None, is_admin = False, is_teacher = False, **extra_fields):
        if not email:
            raise ValueError('usuarios deben contener un correo')
        if not password:
            raise ValueError('usuarios deben contener una contraseña')
        user_obj = self.model(
            email = self.normalize_email(email),
            **extra_fields
        )
        user_obj.set_password(password)
        user_obj.admin = is_admin
        user_obj.teacher = is_teacher
        user_obj.save(using=self._db)
        return user_obj

    def create_superuser(self, email, password = None, **extra_fields):
        user = self.create_user(
            email = email,
            password = password,
            is_admin=True,
            is_teacher=True,
            **extra_fields
        )
        return user

class Users(AbstractBaseUser):
    email = models.EmailField('campo email de usuario',max_length=255,unique=True,error_messages=dict_error_messages)
    name=models.CharField('nombre de usuario',max_length=50,error_messages=dict_error_messages)
    lastname=models.CharField('apellido de usuario',max_length=50,error_messages=dict_error_messages)
    telephone=models.CharField('telefono de usuario',max_length=20,error_messages=dict_error_messages)
    birthdate=models.DateField('fecha de nacimiento de usuario',default=now)
    student = models.BooleanField('si es alumno',default=False)
    teacher = models.BooleanField('si es teacher',default = False)
    admin = models.BooleanField('si es superusuario', default = False)

    is_verified=models.BooleanField('verificacion de correo',default=False)
    verified_date=models.DateField('fecha de verificacion del correo',default='2000-01-01')
    last_modified=models.DateField('fecha que se modifico registro',auto_now=True)


    USERNAME_FIELD = 'email' #username_field y password com campos requeridos
    REQUIRED_FIELDS = ['name','lastname']

    objects = UsersManager()

    def __str__(self):
        full_name = '%s %s' % (self.name, self.lastname)
        return full_name.strip()

    def get_email(self):
        return self.email
    
    def is_student(self):
        return self.student
    
    def is_teacher(self):
        return self.teacher
    
    def is_staff(self):
        return self.teacher
    
    def is_admin(self):
        return self.admin
    
    def has_perm(self, perm, obj=None):
        return self.admin

    def has_module_perms(self, app_label):
        return self.admin