from django.shortcuts import render, redirect
from .models import  Teachers
from Users.models import Users
from Users.serializers import UsersSerializer
from .serializer import TeachersSerializer, GetTeachersSerializer
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response

@api_view(['GET'])
def apiOverview(request):
    api_urls={
        'List':'/teacher-List/',
        'Detail View':'/teacher-Detail/<str:pk>/',
        'Create':'/teacher-Create/',
        'Update':'/teacher-Update/<str:pk>/',
        'Delete':'/teacher-Delete/<str:pk>/',
        'Update Verificate':'/teacher-Verificate/<str:pk>',
    }
    return Response(api_urls)

@api_view(['GET'])
def teacherList(request):
    queryset = Teachers.objects.all()
    serializer = GetTeachersSerializer(queryset,many=True)
    return Response(serializer.data)

@api_view(['GET'])
def teacherDetail(request,pk):
    teacher = Teachers.objects.get(user_id=pk)
    serializer = TeachersSerializer(teacher,many=False)
    return Response(serializer.data)

@api_view(['POST'])
def teacherCreate(request):
    serializer = TeachersSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response({'errors':None,'response':serializer.data},status=200)
    return Response({'errors':serializer.errors},status=400)
