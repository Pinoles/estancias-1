import React, { Component } from "react";
import {Link, useParams} from 'react-router-dom';

export default class formShGroups extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loaded: false,
      placeholder: "Loading"
    };
  }

  componentDidMount() {
    let id = this.props.match.params.id;
    console.log(id);
    fetch("/api/admin/groupSh-Detail/"+id)
      .then(response => {
        if (response.status > 400) {
          return this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            data,
            loaded: true
          };
        });
      });
  }

  render() {
    return (
      <div>
        <h1>Administración de Grupo</h1>
        <Link to="./nuevo/">
            <button  style={{color:"white",backgroundColor:"#355F8D"}} type="button" className="btn btn-primary">
                Nuevo ciclos
            </button>
            <br></br>
        </Link>
        <table className="table table-striped">
            <thead>
            <tr>
              <th scope="col">Dia de la semana</th>
              <th scope="col">Fecha de inicio</th>
              <th scope="col">Fecha de Fin</th>
            </tr>
          </thead>
            <tbody>
                  {this.state.data.map(groupSh => {
                  return (
                      <tr key={groupSh['group_schedule_id']}>
                          <th scope="row">{groupSh['day_of_week1']}</th>
                          <th scope="row">{groupSh['start_time']}</th>
                          <th scope="row">{groupSh['end_time']}</th>
                      </tr>
                  );
                  })}
              </tbody>
        </table>
      </div>
    );
  }
}