from django.db import models

# Create your models here.
dict_error_messages = {'max_length':'muy largo','blank':'no se puede dejar en blanco'}

class Cycles(models.Model):
    cycle_id=models.AutoField(auto_created=True,primary_key=True)
    desc_cycle=models.CharField('descripcion del ciclo',max_length=50,error_messages=dict_error_messages)
    start_date=models.DateField('inicio ciclo',auto_now_add=True)
    end_date=models.DateField('fin ciclo',auto_now_add=True)
    allow_change=models.BooleanField(default=True)

    def __str__(self):
        return self.desc_cycle