# Generated by Django 3.0.7 on 2020-07-20 05:47

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('Students', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='studentsenrollment',
            name='student_id',
        ),
        migrations.AddField(
            model_name='studentsenrollment',
            name='user_id',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
    ]
