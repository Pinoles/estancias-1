import React from 'react';

export default function UsuarioModal(props){
    return(
        <div className="modal-dialog modal-dialog-scrollable" role="document">
            <div className="modal-content">
            <div className="modal-header">
                <h5 className="modal-title" id="exampleModalScrollableTitle">Editar Usuario</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div className="modal-body">
                <div>
                    <div className="col">
                    <div  className="form-group row">
                        <label htmlFor="inputName" className="col-sm-2 col-form-label">Nombre</label>
                        <div className="col-sm-10">
                        <input type="text" 
                            className="form-control" 
                            id="inputName" name="name" 
                            placeholder="First Name" 
                            value={props.data.name || ''}
                            onChange={props.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputLSName"className="col-sm-2 col-form-label">Apellidos</label>
                        <div className="col-sm-10">
                        <input type="text"
                            className="form-control" 
                            id="inputLSName" 
                            name="lastname" 
                            placeholder="Last Name" 
                            value={props.data.lastname || ''}
                            onChange={props.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputEmail" className="col-sm-2 col-form-label">Email</label>
                        <div className="col-sm-10">
                        <input type="email" 
                            className="form-control" 
                            id="inputEmail3" 
                            name="email" 
                            placeholder="Email" 
                            value={props.data.email || ''}
                            onChange={props.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputTEL" className="col-sm-2 col-form-label">Telefono</label>
                        <div className="col-sm-10">
                            <input type="tel" className="form-control" id="inputTEL" name="telephone" value={props.data.telephone || ''} onChange={props.changeHandler} placeholder="Telephone"/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputBirthday" className="col-sm-2 col-form-label">Dia de nacimiento</label>
                        <div className="col-sm-1">
                            <input type="date" id="start" name="birthdate"
                            value={props.data.birthdate || ''}
                            min="1960-01-01" max="2020-12-31"
                            onChange={props.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="checkVerified" className="col-sm-2 col-form-label">Verificar</label>
                        <div className="col-sm-1">
                            <input type="checkbox" id="checkVerified" name="is_verified" checked={props.data.is_verified || false} onChange={props.changeHandlerCB}/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="checkStudent" className="col-sm-2 col-form-label">Estudiante</label>
                        <div className="col-sm-1">
                            <input type="checkbox" id="checkStudent" name="student" checked={props.data.student || false} onChange={props.changeHandlerCB}/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="checkteacher" className="col-sm-2 col-form-label">Es profesor</label>
                        <div className="col-sm-1">
                            <input type="checkbox" id="checkteacher" name="teacher" checked={props.data.teacher || false} onChange={props.changeHandlerCB}/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="checkAdmin" className="col-sm-2 col-form-label">Es administrador</label>
                        <div className="col-sm-1">
                            <input type="checkbox" id="checkAdmin" name="admin" checked={props.data.admin || false} onChange={props.changeHandlerCB}/>
                        </div>
                    </div>
                </div>
                    
            </div>
            <div className="modal-footer">
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={props.submitHandler}>Guardar Cambios</button>
            </div>
            </div>
            </div>
        </div>
    );
}
