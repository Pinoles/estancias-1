import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class LoginUser extends Component {
    constructor(props){
        super(props);
        this.state = {
            email:"",
            password:""
        };

        this.changeHandler = this.changeHandler.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
    }

    changeHandler(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    submitHandler(e){
        e.preventDefault();
        console.log(this.state);
        const csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        fetch("/api/Users/userAuth/", {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(this.state), // data can be `string` or {object}!
            headers:{
                'Content-Type': 'application/json',
                'X-CSRFToken':csrf_token
            }
            }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => {if(!response.errors){
                window.location.href = '../';
              }else{
                console.log(response)
              }});
    }

    render() {
        const {email,password} = this.state;
        return (
            <div className="row">
          
              <div className="col-lg-7 col-md-6 col-sm-6 col-12">
                <div style={{position: "relative"}}>
                   <div style={{position: "absolute", marginTop: "10%",
                       padding: "90px",
                       color: "#fff",
                       width:"100%",
                        backgroundColor:"#2c4984",
                        opacity:"50%",fontWeight: "300"}} className="login-main-text">
                          <h2>ESCUELA DE LENGUAS<br/>DEL ESTADO DE</h2>
                          <h2>DURANGO</h2>
                         
                       </div>
                 </div>
                <img alt="Silverfox" className="img-fluid img-thumbnail" style={{height:"100%",maxWidth: "100%"}}src="https://ep01.epimg.net/elpais/imagenes/2019/12/03/ciencia/1575367828_426213_1575376178_noticia_normal.jpg"/>
              
             </div>
              
              <div style={{height:"100%",alignItems: "center"}}className="col-lg-5 col-md-6 col-sm-6 col-0">
                <form onSubmit={this.submitHandler}
                style={{marginTop: "10%", marginRight:"1rem"}}>
                          <div className="form-group" style={{backgroundColor:"#fff"}}>
                            <div className="container-fluid">
                              <label><b>Correo Electronico</b></label>
                              <input
                                style={{border: "0",padding:"0"}}
                                type="text" 
                                className="form-control" 
                                name="email" 
                                value={email} 
                                onChange={this.changeHandler} 
                                placeholder="Ingrese Correo Electronico"/>
                             </div>
                           </div>
                           <div className="form-group" style={{backgroundColor:"#fff",marginTop:"10%"}}>
                              <div className="container-fluid">
                              <label><b>Contraseña</b></label>
                              <input 
                                style={{border: "0",padding:"0"}}
                                type="password" 
                                className="form-control" 
                                name="password" 
                                value={password} 
                                onChange={this.changeHandler} 
                                placeholder="Ingrese Contraseña"/>
                             </div>
                           </div>
                           <div className="form-group" style={{marginTop:"10%"}}>
                              <button style={{backgroundColor:"#b41c34",
                               color: "#fff"}}
                               type="submit" className="btn btn-black btn-lg btn-block">Entrar</button>
                              </div>
                           <div className="form-group" style={{marginTop:"2%"}}>
                              <Link to="/registro/">
                               <button className="btn btn-secondary btn-lg btn-block">Registrarse</button>
                              </Link>
                           </div>
                        </form>
              </div>
            </div>
        );
    }
}