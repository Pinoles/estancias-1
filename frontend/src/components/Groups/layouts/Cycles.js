import React, { Component } from 'react'

export default class Cycles extends Component{
    constructor(props) {
        super(props);
        this.state = {
          data: [],
          loaded: false,
          placeholder: "Loading"
        };
      }
    
      componentDidMount() {
        fetch("/api/Cycles/cycles-list/")
          .then(response => {
            if (response.status > 400) {
              return this.setState(() => {
                return { placeholder: "Something went wrong!" };
              });
            }
            return response.json();
          })
          .then(data => {
            this.setState(() => {
              return {
                data,
                loaded: true
              };
            });
          });
      }
    render(){
        return(this.state.data.map(cycles => {
                return (
                    <option value={cycles.cycle_id}>{cycles.desc_cycle}</option>
                );
                })
        );
    }
}
