from django.shortcuts import render,redirect
from administration.models import  Groups, GroupsSchedule, PreRegisterGroup
from django.http import HttpResponse
from .serializers import (GroupsSerializer,GetGroupsSerializer, 
    GroupsScheduleSerializer, PreRegisterGroupSerializers,GETPreRegisterGroupSerializers)
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

@api_view(['GET'])
def apiOverview(request):
    api_urls={
        'List':'/group-List/',
        'Detail View':'/group-Detail/<str:pk>/',
        'Create':'/group-Create/',
        'Update':'/group-Update/<str:pk>/',
        'Delete':'/group-Delete/<str:pk>/',
        'Update Verificate':'/group-Verificate/<str:pk>',
    }
    return Response(api_urls)

@api_view(['GET'])
def groupList(request):
    queryset = Groups.objects.all().order_by('level')
    serializer = GetGroupsSerializer(queryset,many=True)
    return Response(serializer.data)

@api_view(['GET'])
def groupDetail(request,pk):
    group = Groups.objects.get(group_id=pk)
    serializer = GetGroupsSerializer(group,many=False)
    return Response(serializer.data)

@api_view(['POST'])
def groupCreate(request):
    serializer = GroupsSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)


@api_view(['GET'])
def apiOverviewSchedule(request):
    api_urls={
        'List':'/groupSh-List/',
        'Detail View':'/groupSh-Detail/<str:pk>/',
        'Create':'/groupSh-Create/',
        'Update':'/groupSh-Update/<str:pk>/',
        'Delete':'/groupSh-Delete/<str:pk>/',
        'Update Verificate':'/groupSh-Verificate/<str:pk>',
    }
    return Response(api_urls)

@api_view(['GET'])
def groupShList(request):
    queryset = GroupsSchedule.objects.all()
    serializer = GroupsScheduleSerializer(queryset,many=True)
    return Response(serializer.data)

@api_view(['GET'])
def groupShDetail(request,pk):
    group = GroupsSchedule.objects.filter(group_id=pk)
    serializer = GroupsScheduleSerializer(group,many=True)
    return Response(serializer.data)

@api_view(['POST'])
def groupShCreate(request):
    serializer = GroupsScheduleSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
    return Response(serializer.data)

#########################################

@api_view(['GET'])
def preRegisterList(request):
    queryset = PreRegisterGroup.objects.all()
    serializer = GETPreRegisterGroupSerializers(queryset,many=True)
    return Response(serializer.data)

@api_view(['GET'])
def preRegisterDetail(request,pk):
    pregistrados = PreRegisterGroup.objects.filter(user_id=pk)
    serializer = GETPreRegisterGroupSerializers(pregistrados,many=True)
    return Response(serializer.data)

@api_view(['POST'])
def preRegisterCreate(request):
    serializer = PreRegisterGroupSerializers(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response({'errors':None,'preregister':serializer.data},status=200)
    return Response({'errors':serializer.errors},status=status.HTTP_406_NOT_ACCEPTABLE)