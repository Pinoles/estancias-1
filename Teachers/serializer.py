from rest_framework import serializers
from .models import Teachers

class GetTeachersSerializer(serializers.ModelSerializer):
    user_id = serializers.StringRelatedField(many=False)
    class Meta:
        model = Teachers
        fields = ('__all__')

class TeachersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teachers
        fields = ('__all__')