from django.apps import AppConfig


class LenguagesConfig(AppConfig):
    name = 'Lenguages'
