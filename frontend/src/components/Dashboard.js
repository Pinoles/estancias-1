import React from 'react';
import { Link } from 'react-router-dom';

export default function Dashboard(props){
    return(
        <div style={{backgroundColor:"#EEEEEE"}} className="d-flex" id="content-wrapper">
            <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet"></link>
                <div style={{backgroundColor:"#FAFAFA",filter:"drop-shadow(2px 4px 3px black)"}}id="sidebar-container" className="bg">
                    <div style={{backgroundColor:"#355F8D",textAlign:"center"}} className="logo">
                        <br></br>
                        <a style={{}} className="text-light text">Panel de Control</a>
                        <br></br>
                        <br></br>
                    </div>
                    <div style={{boxShadow:" 0px 9px 18px #0000002E",textDecoration:"none"}}className="menu">
                        <Link to="/profile/"  style={{color:"#000000DE",textDecoration:"none" }}className="d-block  text-justify p-3 border-0"><i className="icon ion-md-person lead mr-2" ></i>
                            Perfil
                        </Link>
                    {props.data.admin?
                    <div>
                        <a style={{color:"#000000DE",textDecoration:"none" }}href="#" className="d-block  text-justify p-3 border-0"><i className="icon ion-md-apps lead mr-2" ></i>
                            Tablero
                        </a>
                        <a style={{color:"#000000DE",textDecoration:"none" }} href="#" className="d-block text-justify p-3 border-0"><i   className="icon ion-md-stats lead mr-2"></i>
                             Estadísticas
                        </a>
                        <Link to="/usuarios/" style={{color:"#000000DE",textDecoration:"none" }}href="#" className="d-block  text-justify p-3 border-0"><i className="icon ion-md-person lead mr-2"></i>
                            Administracion de usuarios
                        </Link>
                        <Link to="/profesores/"style={{color:"#000000DE",textDecoration:"none" }}  className="d-block text-justify p-3 border-0"><i className="icon ion-md-man lead mr-2"></i>
                            Gestion de Profesores
                        </Link>
                        <Link to="/grupos/" style={{color:"#000000DE",textDecoration:"none" }}className="d-block  text-justify p-3 border-0"><i className="icon ion-md-aperture lead mr-2"></i>
                            Gestion de Grupos
                        </Link>
                        <Link to="/lenguajes/" style={{color:"#000000DE",textDecoration:"none" }}className="d-block  text-justify p-3 border-0"><i className="icon ion-md-globe lead mr-2 "></i>
                            Lenguajes
                        </Link>
                        <Link to="/ciclos/" style={{color:"#000000DE",textDecoration:"none" }} className="d-block  text-justify p-3 border-0"><i className="icon ion-md-filing lead mr-2 "></i>
                            Administración de ciclo
                        </Link>
                        <Link to="/estudiantesEnrrolados/" style={{color:"#000000DE",textDecoration:"none" }} className="d-block  text-justify p-3 border-0">
                            <i className="icon ion-md-filing lead mr-2 "></i>
                            Administracion de Inscritos 
                        </Link>
            
                        <a   style={{color:"#000000DE",textDecoration:"none" }} className="d-block  text-justify p-3 border-0"><i className="icon ion-md-list-box lead mr-2 "></i>
                            Constancia de Termino
                        </a>
                        <a style={{color:"#000000DE",textDecoration:"none" }} href="#" className="d-block  text-justify p-3 border-0"><i className="icon ion-md-calendar lead mr-2 "></i>
                            Calendario
                        </a>  
                        <a  style={{color:"#000000DE",textDecoration:"none" }} href="#" className="d-block  text-justify p-3 border-0"> <i className="icon ion-md-clipboard lead mr-2"></i>
                            Examen de ubicación
                        </a>
                        <a  style={{color:"#000000DE",textDecoration:"none" }} href="#" className="d-block  text-justify p-3 border-0"><i className="icon ion-md-cash lead mr-2"></i>
                            Pagos
                        </a>
                    </div>
                    :null
                }
                {
                    props.data.student?
                    <div>
                        <Link to="/estudiante/grupos/" style={{color:"#000000DE",textDecoration:"none" }} className="d-block  text-justify p-3 border-0">
                            <i   className="icon ion-md-stats lead mr-2"></i>
                            grupos disponibles
                        </Link>
                        <Link to={"/estudiante/gruposPregistrado/"+props.data.id+"/"} style={{color:"#000000DE",textDecoration:"none" }} className="d-block  text-justify p-3 border-0">
                            <i className="icon ion-md-filing lead mr-2 "></i>
                            grupos preregistrado
                        </Link>
                    </div>
                    
                    :null
                }
                </div>
            </div>
        </div>
    );
}