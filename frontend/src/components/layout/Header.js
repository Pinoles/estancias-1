import React from "react";
import { Link } from 'react-router-dom';
const estilo={
  foto:{
    height: '60px',
    textalign: 'left',
  }
}

export default function Header(props){
        return (
          
          <nav  className="navbar navbar-expand-lg  static-top "  style = {{backgroundColor:"#E8F4F9",padding: "20px 0px 30px 0px",overflow:"auto",filter:"drop-shadow(2px 4px 3px black)"}}>
            <div className="container">
              <a style={{top:"0px",left:"0px",position:"absolute"}}className="navbar-brand" href="#">
                    <img style={estilo.foto} src="https://celeujed.com/svg/logo.svg" alt="" ></img> 
                  </a> 
                  <br></br>
              <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                  </button>
              <div  className="collapse navbar-collapse" id="navbarResponsive">
                <ul style={{position:"absolute",top:"2px",right:"0px"}} className="navbar-nav ml-auto">
                  <li className="nav-item"> 
                  <br></br>
                  <i className="icon ion-md-home lead mr-2" ></i>
                  </li>
                  <br></br>
                  <li className="nav-item"> 
                  <br></br>
                  <i className="icon ion-md-search lead mr-2" ></i>
                  </li>
               
                 <Link to="/logout"  onClick={
                   () => {
                    const csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
                    fetch("/logout/", {
                        method: 'POST', // or 'PUT'
                        body: {}, // data can be `string` or {object}!
                        headers:{
                            'Content-Type': 'application/json',
                            'X-CSRFToken':csrf_token
                        }
                        }).then(res => res.json())
                        .catch(error => console.error('Error:', error))
                        .then(response => window.location.reload(true));
                   }
                 }>  
                  <li className="nav-item"> 
                  <br></br>
                  <i  style={{color:"black"}}className="icon ion-md-log-in lead mr-2" ></i>
                  </li> </Link>
                </ul>
              </div>
            </div>
            
          </nav>
        
          );
          
    
}

