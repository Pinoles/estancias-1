import React, { Component } from "react";

export default class teachers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loaded: false,
      placeholder: "Loading"
    };
  }

  componentDidMount() {
    fetch("/api/Teachers/teacher-list/")
      .then(response => {
        if (response.status > 400) {
          return this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            data,
            loaded: true
          };
        });
      });
  }

  render() {
    return (
      <div>
        <h1>Lista de Profesores</h1>
        <a href="http://127.0.0.1:8000/profesores/nuevo">
          <button style={{color:"white",backgroundColor:"#355F8D"}} type="button" className="btn btn">
            Agregar Profesor
          </button>
        </a>
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">Nombre</th>
            </tr>
          </thead>
            <tbody>
                  {this.state.data.map(Teachers => {
                  return (
                      <tr key={Teachers['teacher_id']}>
                          <th scope="row">{Teachers['user_id']}</th>
                      </tr>
                  );
                  })}
              </tbody>
        </table>
      </div>
    );
  }
}
