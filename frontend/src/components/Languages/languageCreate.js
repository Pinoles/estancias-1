import React, { Component } from 'react'

export default class languageCreate extends Component{
    constructor(props){
        super(props);
        this.state = {
            desc_lenguage:"",
            cordinated_by:"",
            data:[],
            placeholder:"Loading",
            loaded:false
        };

        this.changeHandler = this.changeHandler.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
    }

    componentDidMount() {
        fetch("/api/Users/userTeacher/")
          .then(response => {
            if (response.status > 400) {
              return this.setState(() => {
                return { placeholder: "Something went wrong!" };
              });
            }
            return response.json();
          })
          .then(data => {
            this.setState(() => {
              return {
                data,
                loaded: true
              };
            });
          });
      }

    changeHandler(e) {
        this.setState({[e.target.name]: e.target.value});
    }

    submitHandler(e){
        e.preventDefault();
        const csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        fetch("/api/Lenguages/language-Create/", {
            method: 'POST', // or 'PUT'
            body: JSON.stringify({desc_lenguage:this.state.desc_lenguage,
                cordinated_by:this.state.cordinated_by}), // data can be `string` or {object}!
            headers:{
                'Content-Type': 'application/json',
                'X-CSRFToken':csrf_token
            },
            
            }).then(res => {
            if (res.status > 400) {
                this.setState(() => {
                return { placeholder: "Something went wrong!" };
                });
            }
            return res.json();
            })
            .catch(error => console.error('Error:', error))
            .then(response =>
            {if(!response.errors){
                console.log(response);
                this.props.history.goBack();
                }else{
                console.log(response);
                }}
            );
    }

    render(){
        const {desc_lenguage,cordinated_by} = this.state;

        return(
            <div>
              
               <form style={{marginTop: "80px",marginBottom: "80px",backgroundColor:"#EEEEEE"}} className="card text-center" onSubmit={this.submitHandler} >
            <div  className="card text-center card bg-light col-md-8" style={{maxWidth: "680px",marginLeft: "340px"}}>
                <h5 className="card-header">Registrar Lenguaje</h5>
                <div   style={{maxWidth: "680px"}} className="card-body">
                    <div  className="form-group row">
                            <label htmlFor="inputName" className="col-sm-3 col-form-label">Nombre del lenguaje</label>
                            <div className="col-sm-9">
                            <input type="text" 
                                className="form-control" 
                                id="inputName" name="desc_lenguage" 
                                placeholder="descripcion de ciclo" 
                                value={desc_lenguage}
                                onChange={this.changeHandler} />
                            </div>
                        </div>
                    <div className="col">
                        <select className="form-control" value={cordinated_by} name="cordinated_by" onChange={this.changeHandler}>
                            <option value="" selected>-Seleccione el profesor titular-</option>
                            {this.state.data.map(teacher => {
                                return (
                                <option value={teacher.id}>{teacher.name} {teacher.lastname}</option>
                                );
                                })
                            }
                        </select>
                    </div>
                    <div className="form-group row">
                        <div className="col-sm-12 text-center">
                        <button text-align="center" type="submit" className="btn btn-outline-danger btn-lg center-block">Registrar</button>
                        </div>
                    </div>
                </div>
            </div>
            </form> 
            </div>
        );
    }
}