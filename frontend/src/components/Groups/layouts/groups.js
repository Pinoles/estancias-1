import React, { Component } from 'react'
import {Link} from 'react-router-dom';

export default class Groups extends Component{
    constructor(props) {
        super(props);
        this.state = {
          data: [],
          loaded: false,
          placeholder: "Loading"
        };
      }
    
      componentDidMount() {
        fetch("/api/admin/group-list/")
          .then(response => {
            if (response.status > 400) {
              return this.setState(() => {
                return { placeholder: "Something went wrong!" };
              });
            }
            return response.json();
          })
          .then(data => {
            this.setState(() => {
              return {
                data,
                loaded: true
              };
            });
          });
      }

    render(){
        return(
            
            <tbody>
                {this.state.data.map(group => {
                return (
                    <tr key={group.group_id} className="bg-white">
                        <td>
                          <h6>
                          {group.desc_group}
                          </h6>
                          <p class="font-weight-light text-secondary" style={{fontSize:"70%"}}>Profesor: {group.teacher_id}</p>
                        </td>
                        <td>{group.lenguage_id}</td>
                        <td>{group.level}</td>
                        <td>{group.max_capacity}</td>
                        <td>{group.course_type}</td>
                        <td>{group.groupSh[0]}</td>
                        <td>{group.room}</td>
                        <td>
                          <Link to={"./"+group.group_id+"/"}>
                          <button  style={{color:"white",backgroundColor:"#355F8D"}} className="btn btn-sm">Ver Grupo</button>
                          </Link>
                        </td>
                    </tr>
                );
                })}
            </tbody>
        );
    }
}
