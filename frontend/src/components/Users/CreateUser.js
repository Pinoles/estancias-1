import React, { Component } from 'react'

export default class userCreate extends Component{
    constructor(props){
        super(props);
        this.state = {
            name:"",
            lastname:"",
            email:"",
            password:"",
            confimpassword:"",
            telephone:"",
            birthdate:"2018-07-22",
            user_id:"",
            is_verified:false,
            student:false,
            teacher:false,
            admin:false
        };

        this.changeHandler = this.changeHandler.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
        this.changeHandlerCB = this.changeHandlerCB.bind(this);
    }

    changeHandler(e) {
        this.setState({[e.target.name]: e.target.value});
    }
    changeHandlerCB(e) {
        this.setState({[e.target.name]: e.target.checked});
    }

    submitHandler(e){
        e.preventDefault();
        console.log(this.state);
        const csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        if(this.state.confimpassword == this.state.password){
            fetch("/api/Users/userCreate/", {
                method: 'POST', // or 'PUT'
                body: JSON.stringify(this.state), // data can be `string` or {object}!
                headers:{
                  'Content-Type': 'application/json',
                  'X-CSRFToken':csrf_token
                },
                
              }).then(res => {
                if (res.status > 400) {
                   this.setState(() => {
                    return { placeholder: "Something went wrong!" };
                  });
                }
                return res.json();
              })
              .catch(error => console.error('Error:', error))
              .then(response =>
                {if(!response.errors){
                    this.props.history.goBack();
                  }else{
                    console.log(response);
                  }}
              );
        }
    }

    render(){
        const {name,lastname,email,password,confimpassword,telephone,birthdate,is_verified,teacher,admin,student} = this.state;

        return(
            <div>
               
               <form style={{marginTop: "80px",marginBottom: "80px",background:"#EEEEEE"}} className="card text-center" onSubmit={this.submitHandler} >

            <div  className="card text-center card bg-light col-md-13" style={{maxWidth: "680px",marginLeft: "340px",backgroundColor:"#EEEEEE"}}>
                <h5 className="card-header">Registrar Usuario</h5>
                <div   style={{maxWidth: "680px"}} className="card-body">
                    <div  className="form-group row">
                        <label htmlFor="inputName" className="col-sm-3 col-form-label">Nombre</label>
                        <div className="col-sm-9">
                        <input type="text" 
                            className="form-control" 
                            id="inputName" name="name" 
                            placeholder="Ej. Jose Manuel" 
                            value={name}
                            onChange={this.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputLSName"className="col-sm-3 col-form-label">Apellidos</label>
                        <div className="col-sm-9">
                        <input type="text"
                            className="form-control" 
                            id="inputLastName" 
                            name="lastname" 
                            placeholder="Ej. Perez Martinez" 
                            value={lastname}
                            onChange={this.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputEmail" className="col-sm-3 col-form-label">Email</label>
                        <div className="col-sm-9">
                        <input type="email" 
                            className="form-control" 
                            id="inputEmail3" 
                            name="email" 
                            placeholder="Ej. Example@gmail.com" 
                            value={email}
                            onChange={this.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputPassword" className="col-sm-3 col-form-label">Contraseña</label>
                        <div className="col-sm-9">
                        <input type="password" 
                            className="form-control" 
                            id="inputPassword" 
                            name="password" 
                            placeholder="Ingresa una contraseña de al menos 8 caracteres" 
                            value={password}
                            onChange={this.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputPassword" className="col-sm-3 col-form-label">Confirmar contraseña</label>
                        <div className="col-sm-9">
                        <input type="password" 
                            className="form-control" 
                            id="inputConfirmPassword" 
                            name="confimpassword" 
                            placeholder="Vuelve a escribir la contraseña"
                            value={confimpassword}
                            onChange={this.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputTEL" className="col-sm-3 col-form-label">Telefono</label>
                        <div className="col-sm-9">
                        <input type="tel" className="form-control" id="inputTEL" name="telephone" value={telephone} onChange={this.changeHandler} placeholder="Ej. 6180000000"/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputBirthday" className="col-sm-3 col-form-label">Dia de nacimiento</label>
                        <div className="col-sm-1">
                            <input type="date" id="start" name="Ingresar fecha de nacimiento"
                            value={birthdate}
                            min="1960-01-01" max="2020-12-31"
                            onChange={this.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="checkVerified" className="col-sm-3 col-form-label">Verificacion</label>
                        <div className="col-sm-1">
                            <input type="checkbox" id="checkVerified" name="is_verified" checked={is_verified} onChange={this.changeHandlerCB}/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="checkStudent" className="col-sm-3 col-form-label">Es Estudiante</label>
                        <div className="col-sm-1">
                            <input type="checkbox" id="checkStudent" name="student" checked={student} onChange={this.changeHandlerCB}/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="checkteacher" className="col-sm-3 col-form-label">Es profesor</label>
                        <div className="col-sm-1">
                            <input type="checkbox" id="checkteacher" name="teacher" checked={teacher} onChange={this.changeHandlerCB}/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="checkAdmin" className="col-sm-3 col-form-label">Es administrador</label>
                        <div className="col-sm-1">
                            <input type="checkbox" id="checkAdmin" name="admin" checked={admin} onChange={this.changeHandlerCB}/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <div className="col-sm-12 text-center">
                        <button text-align="center" type="submit" className="btn btn-outline-danger btn-lg center-block">Registrar</button>
                        </div>
                    </div>
                </div>
            </div>
            </form> 
            </div>
        );
    }
}