from django.urls import path
from . import views

urlpatterns = [
    path('group-list/', views.groupList, name="group-List"),
    path('group-Detail/<str:pk>/', views.groupDetail, name="group-Detail"),
    path('group-Create/', views.groupCreate, name="group-Create"),
    path('groupSh-list/', views.groupShList, name="groupSh-List"),
    path('groupSh-Detail/<str:pk>/', views.groupShDetail, name="groupSh-Detail"),
    path('groupSh-Create/', views.groupShCreate, name="groupSh-Create"),
    path('preRegister-list/', views.preRegisterList, name="preRegister-List"),
    path('preRegister-Detail/<str:pk>/', views.preRegisterDetail, name="preRegister-Detail"),
    path('preRegister-Create/', views.preRegisterCreate, name="preRegister-Create"),
]