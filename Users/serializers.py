from rest_framework import serializers
from .models import Users

class UsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ('__all__')
        extra_kwargs= {
            'password':{
                'write_only':True
            }
        }

class UsersLogin(serializers.ModelSerializer):
    email = serializers.CharField(trim_whitespace=True, max_length=60)
    password = serializers.CharField(trim_whitespace=True, max_length=60)

    class Meta:
        model = Users
        fields = ('email','password')

class UserRegistrationSerializer(serializers.ModelSerializer):
    confimpassword = serializers.CharField(style={'input_type':'password'}, write_only=True)

    class Meta:
        model = Users
        fields = ('__all__')
        extra_kwargs = {
            'password': {'write_only':True}
        }
    
    def save(self):
        print(self.validated_data)
        usuario = Users(
            email=self.validated_data['email'],
            name=self.validated_data['name'],
            lastname=self.validated_data['lastname'],
            telephone=self.validated_data['telephone'],
            birthdate=self.validated_data['birthdate'],
            student=self.validated_data['student'],
            teacher=self.validated_data['teacher'],
            admin=self.validated_data['admin'],
            is_verified=self.validated_data['is_verified']
            )
        password = self.validated_data['password']
        confimpassword = self.validated_data['confimpassword']

        if password != confimpassword:
            return serializers.ValidationError({'password':'contrasenias deben ser iguales'})
        usuario.set_password(password)
        usuario.save()