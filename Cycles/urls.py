from django.urls import path
from . import views

urlpatterns = [
    path('cycles-list/', views.cyclesList, name="cycles-List"),
    path('cycles-Detail/<str:pk>/', views.cyclesDetail, name="cycles-Detail"),
    path('cycles-Create/', views.cyclesCreate, name="cycles-Create"),
]