from django.contrib import admin
from .models import Students, StudentsEnrollment
# Register your models here.
modelos = [StudentsEnrollment, Students]
admin.site.register(modelos)