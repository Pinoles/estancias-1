import React, { Component } from 'react'

export default class Lenguages extends Component{
    constructor(props) {
        super(props);
        this.state = {
          data: [],
          loaded: false,
          placeholder: "Loading"
        };
      }
    
      componentDidMount() {
        fetch("/api/Lenguages/language-List/")
          .then(response => {
            if (response.status > 400) {
              return this.setState(() => {
                return { placeholder: "Something went wrong!" };
              });
            }
            return response.json();
          })
          .then(data => {
            this.setState(() => {
              return {
                data,
                loaded: true
              };
            });
          });
      }
    render(){
        return(this.state.data.map(lenguages => {
                return (
                    <option value={lenguages.lenguage_id}>{lenguages.desc_lenguage}</option>
                );
                })
        );
    }
}
