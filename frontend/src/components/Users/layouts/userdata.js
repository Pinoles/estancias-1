import React from 'react';

export default function Userdata(props){
    return(
        <div style={{marginTop: "80px",marginBottom: "80px",background:"#EEEEEE"}} className="card text-center" >
            
            <div  className="card text-center card bg-light col-md-8" style={{maxWidth: "680px",marginLeft: "340px"}}>
                <h5 className="card-header">REGISTRO DE USUARIO</h5>
                <div   style={{maxWidth: "680px"}} className="card-body">
                    <div className="form-group row">
                    <div className="col-sm-12">
                        <img src="https://cdn.icon-icons.com/icons2/1508/PNG/512/systemusers_104569.png">
                        </img>
                        </div>
                    </div>
                    <div  className="form-group row">
                        <label htmlFor="inputName" className="col-sm-2 col-form-label">Nombre</label>
                        <div className="col-sm-10">
                        <input type="text" 
                            className="form-control" 
                            id="inputName" name="name" 
                            placeholder="First Name" 
                            value={props.data.name}
                            onChange={props.handleChange}
                            disabled = {(!props.editable)? "disabled" : ""} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputLSName"className="col-sm-2 col-form-label">Apellidos</label>
                        <div className="col-sm-10">
                        <input type="text"
                            className="form-control" 
                            id="inputLastName" 
                            name="lastname" 
                            placeholder="Last Name" 
                            value={props.data.lastname}
                            onChange={props.handleChange}
                            disabled = {(!props.editable)? "disabled" : ""}  />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputEmail" className="col-sm-2 col-form-label">Email</label>
                        <div className="col-sm-10">
                        <input type="email" 
                            className="form-control" 
                            id="inputEmail3" 
                            name="email" 
                            placeholder="Email" 
                            value={props.data.email}
                            onChange={props.handleChange} 
                            disabled = {(!props.editable)? "disabled" : ""} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputTEL" className="col-sm-2 col-form-label">Telefono</label>
                        <div className="col-sm-10">
                        <input type="tel" 
                            className="form-control" 
                            id="inputTEL" 
                            name="telephone" 
                            value={props.data.telephone} 
                            onChange={props.handleChange} 
                            disabled = {(!props.editable)? "disabled" : ""} 
                            placeholder="Telephone"/>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="inputBirthday" className="col-sm-3 col-form-label">Fecha de Nacimiento</label>
                        <div className="col-sm-1">
                            <input type="date" id="start" name="birthdate"
                            value={props.data.birthdate}
                            min="1960-01-01" max="2020-12-31"
                            onChange={props.handleChange} 
                            disabled = {(!props.editable)? "disabled" : ""} />
                        </div>
                    </div>
                    <div className="form-group row">
                        {props.editable?
                            <div className="col-sm-12 text-center">
                                <button className="btn btn-danger" onClick={props.cancel}>Cancelar</button>
                                <button text-align="center" className="btn btn-outline-success btn-lg center-block" onClick={props.submitHandler}>Aceptar</button>
                            </div>
                            : 
                            <div className="col-sm-12 text-center">
                                <button className="btn btn-success" onClick={props.changeEdit}>Editar informacion</button>
                            </div>
                        }
                    </div>
                </div>
            </div>
            </div>
    );
}