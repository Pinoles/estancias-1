import React, { Profiler } from 'react';
import { Switch , Route} from 'react-router-dom';
import PostRegistro from './components/Users/PostRegistro';
import formGroups from './components/Groups/formGroups';
import LoginUser from './components/Users/LoginUser';
import profile from './components/Users/Profile';
import Users from './components/Users/Usuarios';
import userCreate from './components/Users/CreateUser';
import cycles from './components/Cycles/cycles';
import cycleCreate from './components/Cycles/createCycle';
import teachers from './components/Teacher/teachers';
import teacherCreate from './components/Teacher/createTeacher';
import languages from './components/Languages/languages';
import languageCreate from './components/Languages/languageCreate';
import formShGroups from './components/Groups/formGroupsSh';
import studentformGroups from './components/Groups/formGroupsStudent';
import groupShCreate from './components/Groups/groupShCreate';
import StudentsEnrollment from './components/Students/StudentsEnrolment';
import studentformGroupsPre from './components/Groups/groupsPreStudent';
import PagoExamen from './components/Examens/Pagos';
import Preinscripciongrupo from './components/Groups/Preinscripciongrupo';

const Routes = () => {
    return(
        <Switch>
            <Route exact path="/" component={null} />
            <Route exact path="/Registro/" component={PostRegistro} />
            <Route exact path="/grupos/" component={formGroups}/>
            <Route exact path="/grupos/:id/" component={formShGroups}/>
            <Route exact path="/grupos/:id/nuevo/" component={groupShCreate}/>
            <Route exact path="/login/" component={LoginUser} />
            <Route exact path="/profile/" component={profile}/>
            <Route exact path="/usuarios/" component={Users}/>
            <Route exact path="/usuarios/nuevo/" component={userCreate}/>
            <Route exact path="/ciclos/" component={cycles}/>
            <Route exact path="/ciclos/nuevo/" component={cycleCreate}/>
            <Route exact path="/profesores/" component={teachers}/>
            <Route exact path="/profesores/nuevo/" component={teacherCreate}/>
            <Route exact path="/lenguajes/" component={languages}/>
            <Route exact path="/lenguajes/nuevo/" component={languageCreate}/>
            <Route exact path="/estudiante/grupos/" component={studentformGroups}/>
            <Route exact path="/estudiante/grupos/:id/" component={Preinscripciongrupo}/>
            <Route exact path="/estudiantesEnrrolados/" component={StudentsEnrollment}/>
            <Route exact path="/estudiantesEnrrolados/Todos/" component={StudentsEnrollment} />
            <Route exact path="/estudiantesEnrrolados/Grupos/" component={StudentsEnrollment} />
            <Route exact path="/estudiantesEnrrolados/Grupos/:id/" component={StudentsEnrollment} />
            <Route exact path="/estudiantesEnrrolados/Lenguajes/" component={StudentsEnrollment} />
            <Route exact path="/estudiantesEnrrolados/Lenguajes/:id/" component={StudentsEnrollment} />
            <Route exact path="/estudiantesEnrrolados/Modalidades/" component={StudentsEnrollment} />
            <Route exact path="/estudiantesEnrrolados/Modalidades/:id/" component={StudentsEnrollment} />
            <Route exact path="/estudiante/gruposPregistrado/:id/" component={studentformGroupsPre}/>
            <Route exact path="/pagoExamen/" component={PagoExamen}/>
            <Route exact path="/pago/tarjeta/" component={null}/>
        </Switch>
    );
}

export default Routes;