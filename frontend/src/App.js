import React, { Component } from "react";
import { render } from "react-dom";
import Header from "./components/layout/Header";
import Footer from "./components/layout/Footer";
import Routes from "./Routes";
import Dashboard from './components/Dashboard'
import {BrowserRouter } from 'react-router-dom';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loaded: false
    };
  }

  componentDidMount() {
    fetch("/api/Users/userLoget")
      .then(response => {
        if (response.status >= 400) {
          this.setState(() => {
            return { loaded: false};
          });
        }else{
          this.setState(() => {
            return { loaded: true};
          });
        }
        return response.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            data
          };
        });
      });
  }

  render() {
    return (
      <div  style={{backgroundColor:"#EEEEEE"}}> 
        <Header></Header>
        <div className="row mr-0">
          {this.state.loaded?
            <div className="col-2 no-gutters" >
              <Dashboard data={this.state.data}></Dashboard>
            </div>
            :null
          }
          
          <div className="col no-gutters">
            <Routes></Routes>
          </div>
        </div>
        <Footer></Footer>
      </div>
    );
  }
}

export default App;

const container = document.getElementById("app");
render(<BrowserRouter><App /></BrowserRouter>, container);