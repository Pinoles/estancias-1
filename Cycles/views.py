from django.shortcuts import render,redirect
from .models import Cycles
from .serializer import CyclesSerializer
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response

@api_view(['GET'])
def apiOverview(request):
    api_urls={
        'List':'/cycles-List/',
        'Detail View':'/cycles-Detail/<str:pk>/',
        'Create':'/cycles-Create/',
        'Update':'/cycles-Update/<str:pk>/',
        'Delete':'/cycles-Delete/<str:pk>/',
        'Update Verificate':'/cycles-Verificate/<str:pk>',
    }
    return Response(api_urls)

@api_view(['GET'])
def cyclesList(request):
    queryset =Cycles.objects.all()
    serializer =CyclesSerializer(queryset,many=True)
    return Response(serializer.data)

@api_view(['GET'])
def cyclesDetail(request,pk):
    cycles =Cycles.objects.get(cycles_id=pk)
    serializer =CyclesSerializer(cycles,many=False)
    return Response(serializer.data)

@api_view(['POST'])
def cyclesCreate(request):
    serializer =CyclesSerializer(data=request.data)
    if serializer.is_valid():
       serializer.save()
    return Response(serializer.data)