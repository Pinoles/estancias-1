from django.contrib.auth.backends import BaseBackend
from Users.models import Users

class UserBackend(BaseBackend):
    def authenticate(self,request,email,password):
        user_email = email
        user_password = password
        try:
            user = Users.objects.get(email=user_email)
            if user.check_password(user_password) is True:
                return user
        except:
            pass

    def get_user(self, user_id):
        try:
            return Users.objects.get(pk=user_id)
        except:
            return None