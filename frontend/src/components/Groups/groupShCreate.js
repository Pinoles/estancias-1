import React, { Component } from 'react'

export default class groupShCreate extends Component{
    constructor(props){
        super(props);
        let id = this.props.match.params.id;
        this.state = {
            groupSh:{
                group_id:id,
                day_of_week:"",
                start_time:"",
                end_time:""
            },
            data:[],
            placeholder:"Loading",
            loaded:false
        };

        this.changeHandler = this.changeHandler.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
    }

    

    changeHandler(e) {
        let newData = this.state.groupSh;
        newData[e.target.name] = e.target.value;
        this.setState({groupSh:newData});
    }

    submitHandler(e){
        e.preventDefault();
        console.log(this.state)
        const csrf_token = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        fetch("/api/admin/groupSh-Create/", {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(this.state.groupSh), // data can be `string` or {object}!
            headers:{
                'Content-Type': 'application/json',
                'X-CSRFToken':csrf_token
            },
            
            }).then(res => {
            if (res.status > 400) {
                 this.setState(() => {
                return { placeholder: "Something went wrong!" };
                });
            }
            return res.json();
            })
            .catch(error => console.error('Error:', error))
            .then(response =>
            {if(!response.errors){
                console.log(response);
                
                }else{
                console.log(response);
                }}
            );
    
    }

    render(){
        const {group_id, day_of_week, start_time, end_time} = this.state.groupSh;

        return(
            <div>
               
               <div style={{marginTop: "80px",marginBottom: "80px",backgroundColor:"#EEEEEE"}} className="card text-center" >

            <div  className="card text-center card bg-light col-md-8" style={{maxWidth: "680px",marginLeft: "340px"}}>
                <h5 className="card-header">Registro de calendario grupo</h5>
                <div   style={{maxWidth: "680px"}} className="card-body">
                    <div className="col">
                        <label htmlFor="day_week" className="col-sm-10">Dia de la Semana</label>
                        <select id="day_week" className="col-sm-10" value={day_of_week || ""} name="day_of_week"onChange={this.changeHandler}>
                            <option value="Mo">Lunes</option>
                            <option value="Tu" >Martes</option>
                            <option value="We">Miercoles</option>
                            <option value="Th">Jueves</option>
                            <option value="Fr" >Viernes</option>
                            <option value="Sa">Sabado</option>
                            <option value="Su">Domingo</option>
                        </select>
                    </div>
                    <div className="col">
                        <label htmlFor="inputStart" className="col-sm-2 col-form-label">Hora que empieza</label>
                        <div className="col-sm-10">
                            <input type="time" className="col-sm-10" id="inputStart" name="start_time" value={start_time || ''} onChange={this.changeHandler} />
                        </div>
                    </div>
                    <div className="col">
                        <label htmlFor="inputEnd" className="col-sm-2 col-form-label">Hora que Termina</label>
                        <div className="col-sm-10">
                            <input type="time" className="col-sm-10" id="inputEnd" name="end_time" value={end_time || ''} onChange={this.changeHandler} />
                        </div>
                    </div>
                    <div className="form-group row">
                   
                        <div style={{paddingTop:"10px"}} className="col-sm-12 text-center">
                        <button text-align="center" type="submit" className="btn btn-outline-danger btn-lg center-block" onClick={this.submitHandler}>Registrar</button>
                        </div>
                    </div>
                </div>
            </div>
            </div> 
            </div>
        );
    }
}