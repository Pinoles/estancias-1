import React, { Component } from "react";
import {Link} from 'react-router-dom';

export default class languages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loaded: false,
      placeholder: "Loading"
    };
  }

  componentDidMount() {
    fetch("/api/Lenguages/language-List/")
      .then(response => {
        if (response.status > 400) {
          return this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            data,
            loaded: true
          };
        });
      });
  }

  render() {
    return (
      <div>
        <h1>Administración de Lenguajes</h1>
        <Link to="./nuevo">
          <button  style={{color:"white",backgroundColor:"#355F8D"}} type="button" className="btn btn">
            Nuevo Lenguaje
          </button>
        </Link>
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">Descripcion</th>
              <th scope="col">Cordinado por</th>
            </tr>
          </thead>
            <tbody>
                  {this.state.data.map(language => {
                  return (
                      <tr key={language['lenguage_id']}>
                            <th scope="row">{language['desc_lenguage']}</th>
                            <th scope="row">{language['cordinated_by']}</th>
                      </tr>
                  );
                  })}
              </tbody>
        </table>
      </div>
    );
  }
}