from django.shortcuts import render,redirect
from .models import LenguagesCourses
from .serializer import  LanguageSerializer, GETLanguageSerializer
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response



@api_view(['GET'])
def apiOverview(request):
    api_urls={
        'List':'/language-List/',
        'Detail View':'/language-Detail/<str:pk>/',
        'Create':'/langueage-Create/',
        'Update':'/language-Update/<str:pk>/',
        'Delete':'/lenguage-Delete/<str:pk>/',
        'Update Verificate':'/lenguage-Verificate/<str:pk>',
    }
    return Response(api_urls)

@api_view(['GET'])
def languageList(request):
    queryset = LenguagesCourses.objects.all().order_by('desc_lenguage')
    serializer = GETLanguageSerializer(queryset,many=True)
    return Response(serializer.data)

@api_view(['GET'])
def languageDetail(request,pk):
    Lenguage = LenguagesCourses.objects.get(lenguage_id=pk)
    serializer = GETLanguageSerializer(Lenguage,many=False)
    return Response(serializer.data)

@api_view(['POST'])
def languageCreate(request):
    serializer = LanguageSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        print("Debio de funcar")
    return Response(serializer.data)